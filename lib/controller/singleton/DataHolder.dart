import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:my_proyect/model/Partida.dart';

import '../../model/Game.dart';
import '../../model/PerfilUser.dart';
import '../../views/style_views/platform_data.dart';
import '../FbAdmin.dart';
import '../../model/PerfilPro.dart';


class DataHolder{

  static final DataHolder _dataHolder = DataHolder._internal();

  FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseFirestore db = FirebaseFirestore.instance;
  FirebaseStorage storage = FirebaseStorage.instance;


  User? user;
  String verificationPhone = "";
  PerfilPro perfil= PerfilPro();
  PerfilUser perfilUser= PerfilUser();
  List <PerfilUser> users = [];

  final  List games = [];
  final  List puntos = [];
  final Game game = Game();
  final Partida partidas = Partida();
  late  Game selectedGames = Game();


  FBAdmin fbAdmin = FBAdmin();


  late PlatformAdmin platformAdmin;

  DataHolder._internal() {

    platformAdmin=PlatformAdmin();
  }

  factory DataHolder(){
    return _dataHolder;
  }

}

