import 'package:flutter/cupertino.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import '../views/games_views/Calculo/Levels/levels_calculo_view.dart';
import '../views/games_views/Frase/levels_phrases_view.dart';
import '../views/games_views/Memory/Levels/levels_memory_view.dart';
import '../views/games_views/PhotoMemory/Levels/levels_photo_memory_view.dart';
import '../views/games_views/Quiz/Levels/levels_quiz_view.dart';
import '../views/games_views/Simon/Levels/levels_simon_view.dart';

class AdminGames{

  void listItemShortClicked(int index) {
    DataHolder().selectedGames = DataHolder().games[index];
    print('Juego seleccionado: ${DataHolder().selectedGames.uid}');
  }

  int pagesIndex=0;

   List<Widget> pages=[

    const NivelesPhotoMemoryView(),
    const NivelesMemoryView(),
    const NivelesSimonView(),
    NivelesCalculoView(),
    const NivelesFrasesView(),
    const NivelesQuizView(),
  ];

  Future<void> agregarPartida(String userId, String gameId, Map<String, dynamic> partidaData) async {
  try {
    final userRef = DataHolder().db.collection('usuarios').doc(userId);
    final gamesRef = userRef.collection('games').doc(gameId);
    final partidasRef = gamesRef.collection('partidas');

    await partidasRef.add(partidaData);
    print('Partida agregada con éxito');
  } catch (e) {
    print('Error al agregar la partida: $e');
  }
}



  }










