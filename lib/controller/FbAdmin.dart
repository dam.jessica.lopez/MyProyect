import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import 'package:my_proyect/views/components_views/snack.dart';
import '../model/PerfilPro.dart';
import '../model/PerfilUser.dart';
import '../views/authentication/code_view.dart';

class FBAdmin {
  //----------------Métodos para los profesionales---------------------//

  //Método para comprobar si el usuario existe en la colección de profesionales

  Future<bool> downloadProfile() async {
    String? idPerfil = DataHolder().auth.currentUser?.uid;

    try {
      final docRef = DataHolder()
          .db
          .collection("profesionales")
          .doc(idPerfil)
          .withConverter(
        fromFirestore: PerfilPro.fromFirestore,
        toFirestore: (PerfilPro perfil, _) => perfil.toFirestore(),
      );

      DocumentSnapshot docSnap = await docRef.get();
      return docSnap.exists;
    } on FirebaseException catch (e) {
      // Manejar errores

      debugPrint("Error en descargarPerfil() -------->${e.code}");

      return false;
    }
  }

  //Método para crear el perfil del profesional en la colección de profesionales

  Future<String> insertProfile(String name, String surname, String profession,
      String? path, BuildContext context) async {
    String message;
    String formatProfession = profession.toLowerCase().trim();
    print(formatProfession);

    try {
      PerfilPro perfil = PerfilPro(
        name: name,
        surname: surname,
        profession: formatProfession,
        image: path,
        isAdmin: formatProfession == "medico" || formatProfession == "médico" ? true : false,
      );

      DataHolder()
          .db
          .collection("profesionales")
          .doc(DataHolder().auth.currentUser?.uid)
          .set(perfil.toFirestore())
          .onError((e, _) => debugPrint("Error writing document: $e"));

      message = 'Perfil de profesional creado exitosamente';

      return message;
    } on FirebaseException catch (e) {
      // Manejar errores
      if (e.code == 'email-already-in-use') {
        message = 'Ya hay una cuenta con ese email';
      } else if (e.code == 'invalid-email') {
        message = 'Email inválido';
      } else {
        message = 'Error al crear el perfil de usuario';
      }

      return message;
    }
  }

  //Método para recuperar el perfil del profesional de la colección de profesionales

  Future<PerfilPro?> getProfile() async {
    PerfilPro? profile;

    try {
      final ref = DataHolder()
          .db
          .collection("profesionales")
          .doc(DataHolder().auth.currentUser?.uid)
          .withConverter(
        fromFirestore: PerfilPro.fromFirestore,
        toFirestore: (PerfilPro profile, _) => profile.toFirestore(),
      );

      final docSnap = await ref.get();

      if (docSnap.exists) {
        profile = docSnap.data()!;
        debugPrint("-----EL NOMBRE--------------->->${profile.name}");
        debugPrint("-----EL APELLIDO--------------->->${profile.surname}");
        debugPrint("-----LA EDAD--------------->->${profile.profession}");
        debugPrint("-----LA RUTA DE IMAGEN--------------->->${profile.image}");

        return profile;
      } else {

        PerfilPro profile = PerfilPro(
          name: "Invitado",
          surname: "Invitado",
          profession: "Indefinida",
          image: null,
          isAdmin: false,
        );
        
        debugPrint("No such document.");

        return profile;
      }
    } catch (e) {
      debugPrint("Error --------> $e");
    }
    return null;
  }

  //Método para loguear al profesional en Firebase Auth

  Future singIn(String emailAddress, String password,
      BuildContext context) async {
    await Future.delayed(const Duration(seconds: 1));

    try {
      await DataHolder().auth.signInWithEmailAndPassword(
        email: emailAddress,
        password: password,
      );

      return null;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No hay ningun usuario con ese email';
      } else if (e.code == 'wrong-password') {
        return 'Contraseña incorrecta';
      } else if (e.code == 'invalid-email') {
        return 'Email inválido';
      } else {
        return 'Error al iniciar sesión';
      }
    }
  }

  //Método para registrar al profesional en Firebase Auth

  Future<void> signUp(String emailAddress, String password,
      BuildContext context) async {
    await DataHolder()
        .auth
        .createUserWithEmailAndPassword(
      email: emailAddress,
      password: password,
    )
        .then((value) => Navigator.pushReplacementNamed(context, '/onBoarding'))
        .onError((FirebaseAuthException error, stackTrace) =>
    {
      if (error.code == 'weak-password')
        {
          showSnackBar(context, 'La contraseña es demasiado débil'),
        }
      else
        if (error.code == 'email-already-in-use')
          {
            showSnackBar(context, 'Ya hay una cuenta con ese email'),
          }
        else
          if (error.code == 'invalid-email')
            {
              showSnackBar(context, 'Email inválido'),
            }
          else
            {
              showSnackBar(context, 'Error al crear la cuenta'),
            }
    });
  }

  //Método para recuperar contraseña del profesional.

  Future resetPassword(String emailAddress, BuildContext context) async {
    
      await DataHolder().auth.sendPasswordResetEmail(email: emailAddress)
      .then((value) => showSnackBar(context, 'Se ha enviado un correo electrónico para restablecer la contraseña'))
      .catchError((error) => showSnackBar(context, 'Error al enviar el correo electrónico para restablecer la contraseña'));
  }

  //Método para borrar un profesional de la colección de profesionales

  Future<void> deleteProfesional(String? id, BuildContext context) async {
    id ?? print("EL ID ES NULO");

    final documentProfile = DataHolder().db.collection("profesionales").doc(id);

    await documentProfile
        .delete();
        
  }


//------------Métodos para los usuarios-----------------//

//Método para crear las credenciales del usuario en Firebase Auth con el teléfono

  void verifyCode({
    required BuildContext context,
    required String verificationId,
    required String code,
    required Function onSuccess,
  }) async {
    try {
      // Obtener el credential a partir del verificationId y el smsCode ingresado por el usuario
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: code,
      );

      // Crear las credenciales del usuario en Firebase Auth

      UserCredential userCredential =
      await DataHolder().auth.signInWithCredential(credential);

      User? user = userCredential.user;

      if (user != null) {
        onSuccess();
      } else {
        debugPrint("EL USUARIO ES NULO");
      }

      debugPrint('Credenciales del usuario creadas correctamente');
    } on FirebaseAuthException catch (e) {
      // Manejar errores en caso de que falle la creación de las credenciales del usuario
      if (e.code == 'invalid-verification-code') {
        showSnackBar(context, 'Código de verificación inválido');
      } else if (e.code == 'invalid-verification-id') {
        showSnackBar(context, 'Id de verificación inválido');
        debugPrint('Id de verificación inválido');
      } else if (e.code == 'session-expired') {
        showSnackBar(context, 'Tiempo de espera de la sesión expirado');
        debugPrint('Tiempo de espera de la sesión expirado');
      } else {
        showSnackBar(context, 'Error al crear las credenciales del usuario');
        debugPrint('Error al crear las credenciales del usuario: $e');
      }
    }
    return null;
  }

//Comprobar si el usuario ya existe en la base de datos

  Future<bool> checkUserExist() async {
    DocumentSnapshot doc = await DataHolder()
        .db
        .collection("usuarios")
        .doc(DataHolder().auth.currentUser?.uid)
        .get();
    if (doc.exists) {
      print("USUARIO EXISTE");
      return true;
    } else {
      print("USUARIO NO EXISTE");
      return false;
    }
  }

// Método para enviar el código de verificación al número de teléfono

  Future<void> signInWithPhone(BuildContext context, String phoneNumber) async {
    try {
      // Enviar el código de verificación al número de teléfono
      await DataHolder().auth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        verificationCompleted: (PhoneAuthCredential credential) async {
          await DataHolder().auth.signInWithCredential(credential);
          debugPrint('Credenciales del usuario creadas correctamente');
        },
        verificationFailed: (FirebaseAuthException exception) {
          // La verificación falló
          if (exception.code == 'invalid-phone-number') {
            debugPrint('El número de teléfono proporcionado no es válido');
          } else if (exception.code == 'too-many-requests') {
            debugPrint('Demasiadas solicitudes enviadas al servidor');
          } else if (exception.code == 'invalid-verification-code') {
            debugPrint('Código de verificación inválido');
          } else {
            debugPrint(
                'Error al verificar el número de teléfono: ${exception
                    .message}');
          }
        },
        codeSent: (String verificationId, int? resendToken) {
          // El código de verificación fue enviado al número de teléfono
          // Puedes guardar el verificationId y utilizarlo en el paso 3
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      CodeView(
                        verificationId: verificationId,
                      )));
          debugPrint(
              'Código de verificación enviado. verificationId: $verificationId');
        },
        codeAutoRetrievalTimeout: (String verificationId) {
          // El tiempo de espera para la recuperación automática del código de verificación ha expirado
          // Puedes manejar esta situación si es necesario
        },
      );
    } on FirebaseAuthException catch (e) {
      // Manejar cualquier excepción aquí
      showSnackBar(context, e.message.toString());
    }
  }

//Método para crear el perfil del usuario en la colección de usuarios

  Future<String> createProfileUser() async {
  String message;

  try {
    // Crear perfil en la colección "usuarios" de Firestore
    PerfilUser perfil = PerfilUser(
      uid: DataHolder().auth.currentUser!.uid,
      name: "",
      surname: "",
      gender: "",
      phone: DataHolder().auth.currentUser?.phoneNumber,
      age: Timestamp.now(),
      createdDate: Timestamp.now(),
      isActive: true,
    );

    await DataHolder()
        .db
        .collection('usuarios')
        .doc(DataHolder().auth.currentUser?.uid)
        .set(perfil.toFirestore());

    debugPrint("Perfil de usuario creado correctamente");
    message = 'Perfil de usuario creado correctamente';

    // Crear la subcolección "games" en el documento de perfil de usuario
    await setGamesSubcollection();

    debugPrint("Subcolección 'games' creada correctamente");
    return message;
  } on FirebaseException catch (e) {
    message = e.code;
    debugPrint("Error en createProfileUser() -------->${e.code}");
    return message;
  }
}

//Método para crear la subcolección games en el perfil del usuario cuando se inserta.

Future<void> setGamesSubcollection() async {
  final gamesCollection = await DataHolder().db.collection('games').get();
  final games = gamesCollection.docs;

  for (var game in games) {
    final gameRef = DataHolder()
        .db
        .collection('usuarios')
        .doc(DataHolder().auth.currentUser?.uid)
        .collection('games')
        .doc(game.id);

    final gameData = {
      'name': game['name'],
      'image': game['image'],
    };

    await gameRef.set(gameData);
    
      // Agregar subcolección "partidas" dentro de cada juego
      final partidasRef = gameRef.collection('partidas');
      await partidasRef.add({});
  }
}


//Método para actualizar el perfil del usuario en la colección de usuarios desde el profesional

 Future <void> updateProfileUser(String? id, String name, String surname, String gender,
      Timestamp age, BuildContext context) async {
    id ?? print("EL ID ES NULO");

    final documentProfile = DataHolder().db.collection("usuarios").doc(id);

   await documentProfile
        .update(
          {
            "name": name,
            "surname": surname,
            "gender": gender,
            "age": age,
          },
        )
        .then((value) => {
              showSnackBar(context, "Perfil actualizado correctamente"),
              Navigator.pop(context)
            })
        .catchError((error) => {
              showSnackBar(context, "Error al actualizar el perfil del usuario")
            });
        }

  //Método para recuperar el perfil del usuario de la colección de usuarios

  Future<PerfilUser?> getProfileUser() async {
    PerfilUser? p;

    try {
      final ref = DataHolder()
          .db
          .collection("usuarios")
          .doc(DataHolder().auth.currentUser?.uid)
          .withConverter(
        fromFirestore: PerfilUser.fromFirestore,
        toFirestore: (PerfilUser profile, _) => profile.toFirestore(),
      );

      final docSnap = await ref.get();

      if (docSnap.data() != null) {
        p = docSnap.data()!;
        debugPrint("-----EL NOMBRE--------------->->${p.name}");
        debugPrint("-----El TELÉFONO--------------->${p.phone}");
        debugPrint("-----EL UID-------------------->${p.uid}");
        return p;
      } else {
        debugPrint("No such document.");

        return p;
      }
    } catch (e) {
      debugPrint("Error --------> $e");
    }
    return null;
  }


  //Método para comprobar el perfil del usuario de la colección de usuarios

  Future<bool> downloadUserProfile() async {
    String? idPerfil = DataHolder().auth.currentUser?.uid;

    try {
      final docRef =
      DataHolder().db.collection("usuarios").doc(idPerfil).withConverter(
        fromFirestore: PerfilPro.fromFirestore,
        toFirestore: (PerfilPro perfil, _) => perfil.toFirestore(),
      );

      DocumentSnapshot docSnap = await docRef.get();
      return docSnap.exists;
    } on FirebaseException catch (e) {
      // Manejar errores

      debugPrint("Error en descargarPerfil() -------->${e.code}");

      return false;
    }
  }

  //Método para borrar un usuario de la colección de usuarios

Future<void> deleteUserAndSubcollections(String userId) async {
  final userRef = DataHolder().db.collection('usuarios').doc(userId);

  final gamesSnapshot = await userRef.collection('games').get();
  for (final gameDoc in gamesSnapshot.docs) {
    final gameRef = userRef.collection('games').doc(gameDoc.id);
    final partidasSnapshot = await gameRef.collection('partidas').get();
    for (final partidaDoc in partidasSnapshot.docs) {
      final partidaRef = gameRef.collection('partidas').doc(partidaDoc.id);
      await partidaRef.delete();
    }
    await gameRef.delete();
  }

  await userRef.delete();
}

  //----------------Métodos comunes---------------------//

  //Método para subir la imagen de perfil del usuario o profesional a Firebase Storage

  Future pickUploadImage() async {
    ImagePicker image = ImagePicker();
    XFile? file = await image.pickImage(source: ImageSource.gallery);

    if (file == null) return;

    String uniqueFileName = DateTime
        .now()
        .millisecondsSinceEpoch
        .toString();
    Reference referenceRoot = DataHolder().storage.ref();
    Reference referenceDirImages = referenceRoot.child('profileImages');

    Reference referenceImageToUpload = referenceDirImages.child(uniqueFileName);

    try {
      await referenceImageToUpload.putFile(File(file.path));
      String uri = await referenceImageToUpload.getDownloadURL();
      debugPrint("------------------URI$uri");
      return uri;
    } catch (error) {
      debugPrint(error.toString());
    }
  }

  //Método para cerrar sesión

  Future <void> singOut(context) async {
    print("Cerrando sesión");
    await DataHolder().auth.signOut();
  }
}