import 'package:cloud_firestore/cloud_firestore.dart';

class Game {
  final String? name;
  final String? image;
  final String uid;


  Game( {
    this.name="",
    this.image="",
    this.uid="",
  });


  factory Game.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options,
      ) {
    final data = snapshot.data();
    return Game(
        name: data?['name'],
        image: data?['image'],
        uid: snapshot.id,
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (name != null) "name": name,
      if (image != null) "image": image,
    };
  }
}