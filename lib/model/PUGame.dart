import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:my_proyect/model/Partida.dart';
import '../controller/singleton/DataHolder.dart';

class PUGame {
  String uid;
  String? name;
  String? image;
  List<Partida> partidas;

  String? get myUid => uid;

  PUGame({
    this.uid = ' ',
    this.name,
    this.image,
    this.partidas = const [],
  });

  factory PUGame.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    final data = snapshot.data();
    return PUGame(
      uid: snapshot.id,
      name: data?['name'],
      image: data?['image'],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (name != null) "name": name,
      if (image != null) "image": image,
    };
  }

  Future<void> getLastPartidasFromFB() async {
    try {
      final partidasRef = DataHolder()
          .db
          .collection(
              'usuarios/${DataHolder().perfilUser.uid}/games/$uid/partidas')
          .orderBy('fecha', descending: true) // Ordenar por fecha descendente
          .limit(1); // Obtener solo el primer documento

      final querySnapshot = await partidasRef.get();

      partidas = querySnapshot.docs
          .map((doc) => Partida.fromFirestore(doc, null))
          .toList();
      print(partidas.map((e) => e.uid).toList());
    } catch (e) {
      print('Error al recuperar las partidas: $e');
    }
  }

  Future<void> getPartidasWeekFromFB(String docUid) async {
    DateTime now = DateTime.now();
    DateTime startOfLastWeek = DateTime(now.year, now.month, now.day)
        .subtract(Duration(days: now.weekday + 6));
    DateTime endOfLastWeek = startOfLastWeek.add(Duration(days: 6));

    try {
      final partidasRef = DataHolder()
          .db
          .collection('usuarios')
          .doc(docUid)
          .collection('games')
          .doc(uid)
          .collection('partidas')
          .where(
            'fecha',
            isGreaterThanOrEqualTo: DateTime.now().subtract(Duration(days: 7)),
          )
          .orderBy('fecha', descending: true);

      final querySnapshot = await partidasRef.get();

      partidas = querySnapshot.docs
          .map((doc) => Partida.fromFirestore(doc, null))
          .toList();

      print(partidas.map((e) => e.uid).toList());

    } catch (e) {
      print('Error al recuperar las partidas: $e');
    }
  }

  Future<void> addMatch(
      String userId, String gameId, Partida partidaData) async {
    await DataHolder()
        .db
        .collection('usuarios')
        .doc(userId)
        .collection('games')
        .doc(gameId)
        .collection('partidas')
        .doc()
        .set(partidaData.toFirestore())
        .then((value) => print('Partida agregada con éxito'))
        .catchError((error) => print('Error al agregar la partida: $error'));
  }
}
