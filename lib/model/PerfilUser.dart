import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import 'PUGame.dart';

class PerfilUser {
  final String uid;
  String? name;
  String? surname;
  String? gender;
  String? phone;
  Timestamp? age;
  Timestamp? createdDate;
  Map<String, PUGame> games;
  bool isActive = true;

  PerfilUser({
    this.uid = ' ',
    this.name,
    this.surname,
    this.gender,
    this.phone,
    this.age,
    this.createdDate,
    this.games=const {},
    this.isActive = true,
  });


  factory PerfilUser.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    final data = snapshot.data();
    return PerfilUser(
      uid: snapshot.id,
      name: data?['name'],
      surname: data?['surname'],
      gender: data?['gender'],
      phone: data?['phone'],
      age: data?['age'],
      createdDate: data?['createdDate'],
      isActive: data?['isActive'],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (name != null) "name": name,
      if (surname != null) "surname": surname,
      if (gender != null) "gender": gender,
      if (phone != null) "phone": phone,
      if (age != null) "age": age,
      if (createdDate != null) "createdDate": createdDate,
      if (isActive != null) "isActive": isActive,
    };
  }

Future<void> getGamesFromFB() async {
    try {
      final gamesRef = DataHolder().db
          .collection('usuarios')
          .doc(uid)
          .collection('games');

      final querySnapshot = await gamesRef.get();

      games = Map.fromEntries(await Future.wait(querySnapshot.docs.map((doc) async {
        final pUGame = PUGame.fromFirestore(doc, null);

        // Obtener el nombre del juego desde la colección principal
        final juegoDoc = await DataHolder().db.collection('games').doc(pUGame.uid).get();
        final nombreJuego = juegoDoc['name'];
        final imagenJuego = juegoDoc['image'];

        pUGame.name = nombreJuego; // Asignar el nombre del juego al campo PUGames
        pUGame.image = imagenJuego; // Asignar la imagen del juego al campo PUGames

        return MapEntry(pUGame.name!, pUGame);
      }).toList()));

      //print(games.values.map((e) => e.uid).toList());
    } catch (e) {
      //print('Error al recuperar la subcolección: $e');
    }
  }


  /*PUGame getPUGame(String uidGame) {
    return games!.firstWhere((game) => game.uid == uidGame);
  }*/
}



