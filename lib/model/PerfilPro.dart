import 'package:cloud_firestore/cloud_firestore.dart';

class PerfilPro {
   String? name;
   String? surname;
   String? profession;
   String? image;
  final bool? isAdmin;
  final String uid;


  PerfilPro(
      {this.name = "",
      this.surname = "",
      this.profession = "",
      this.image = "",
      this.isAdmin = false,
      this.uid = ""});

  factory PerfilPro.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options,
      ) {
    final data = snapshot.data();
    return PerfilPro(
        name: data?['name'],
        surname: data?['surname'],
        profession: data?['profession'],
        image: data?['image'],
        isAdmin: data?['isAdmin'],
        uid: snapshot.id

    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (name != null) "name": name,
      if (surname != null) "surname": surname,
      if (profession != null) "profession": profession,
      if (isAdmin != null) "isAdmin": isAdmin,
      if (image != null) "image": image,
    };
  }
}