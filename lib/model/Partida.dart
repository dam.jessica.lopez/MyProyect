import 'package:cloud_firestore/cloud_firestore.dart';

class Partida {
  Timestamp? fecha;
  int? puntos;
  int? level;
  final String uid;


  Partida( {
    this.fecha,
    this.puntos,
    this.level,
    this.uid=' ',

  });
  void setFecha(Timestamp timestamp) {
    fecha = timestamp;
  }

  void setPuntos(int valorPuntos) {
    puntos = valorPuntos;
  }


  factory Partida.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options,
      ) {
    final data = snapshot.data();
    return Partida(
        fecha: data?['fecha'],
        puntos: data?['puntos'],
        level: data?['level'],
        uid: snapshot.id
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (fecha != null) "fecha": fecha,
      if (puntos != null) "puntos": puntos,
      if (level != null) "level": level,
    };
  }
}