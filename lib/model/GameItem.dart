import 'package:flutter/material.dart';


class GameItem extends StatelessWidget {


  final String ImgUrl;
  final String Name;
  final Function(int index) onShortClick;
  final int index;

  const GameItem({Key? key, required this.ImgUrl, required this.Name,
    required this.onShortClick, required this.index}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onShortClick(index);
      },


      child: Card(
        color: Colors.white,
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(ImgUrl,height:MediaQuery.of(context).size.height*0.15 ),
             SizedBox(
              height:  MediaQuery.of(context).size.height*0.02,
            ),
            Text(Name,style: const TextStyle(fontSize: 30),)
          ],
        ),

      ),
    );
  }
}