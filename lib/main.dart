import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_proyect/views/authentication/auth_profesional/login_view_pro.dart';
import 'package:my_proyect/views/authentication/login_view.dart';
import 'package:my_proyect/views/authentication/auth_profesional/register_view.dart';
import 'package:my_proyect/views/components_views/bar_menu.dart';
import 'package:my_proyect/views/profesional_profile_views/add_user.dart';
import 'package:my_proyect/views/profesional_profile_views/list_user.dart';
import 'package:my_proyect/views/profesional_profile_views/management_profiles.dart';
import 'package:my_proyect/views/profesional_profile_views/profesional_profile.dart';
import 'package:my_proyect/views/user_profiles_views/fav.dart';
import 'package:my_proyect/views/user_profiles_views/usuario_view.dart';
import 'controller/singleton/DataHolder.dart';
import 'views/authentication/auth_profesional/on_boarding_view.dart';
import 'views/user_profiles_views/home_view.dart';
import 'views/games_views/Memory/Levels/Level1/Memory.dart';
import 'views/games_views/Quiz/Levels/Level1/questions_screen.dart';
import 'views/games_views/Quiz/Levels/quiz.dart';
import 'views/authentication/splash_view.dart';
import 'firebase_options.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //const MyApp({super.key});

  MyApp({Key? key}) : super(key: key);
  FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    MaterialApp materialAppMobile;

    if (DataHolder().platformAdmin.isAndroidPlatform() ||
        DataHolder().platformAdmin.isIOSPlatform()) {
      //print("ENTRO EN ANDROID O IOS");
      materialAppMobile = MaterialApp(
        debugShowCheckedModeBanner: false,
        debugShowMaterialGrid: false,
        title: 'JBrain',
        initialRoute: '/splash2',
        routes: {
          '/splash2': (context) => SplashView2(),
          '/login': (context) => LoginView(),
          '/loginPro': (context) => LoginViewPro(),
          '/register': (context) => RegisterView(),
          '/onBoarding': (context) => OnBoardingView(),
          '/homePro': (context) => ProfesionalProfile(),
          '/listUser': (context) => ListUser(),
          '/add_user': (context) => AddUser(),
          '/home': (context) => HomeView(),
          '/bar': (context) => BarMenu(),
          '/user': (context) => UsuarioView(),
           '/man' : (context) => ManagementProfiles(),
         // '/edit': (context) => EditPhoto(),
          '/memory': (context) => Memory(),
          '/quiz': (context) => Quiz(),
          '/quiz2': (context) => QuestionsScreen(),
         // '/fav': (context) => const Fav(),
        },
      );
    } else {
      materialAppMobile = MaterialApp(
        debugShowCheckedModeBanner: false,
        debugShowMaterialGrid: false,
        initialRoute: '/splash2',
        routes: {
          '/splash2': (context) => SplashView2(),
          '/login': (context) => LoginView(),
          '/loginPro': (context) => LoginViewPro(),
          '/register': (context) => RegisterView(),
          '/onBoarding': (context) => OnBoardingView(),
          '/homePro': (context) => ProfesionalProfile(),
          '/listUser': (context) => ListUser(),
          '/add_user': (context) => AddUser(),
          '/man' : (context) => ManagementProfiles(),
          '/home': (context) => HomeView(),
          '/bar': (context) => BarMenu(),
          '/user': (context) => UsuarioView(),
          //'/edit': (context) => EditPhoto(),
          '/memory': (context) => Memory(),
          '/quiz': (context) => Quiz(),
          '/quiz2': (context) => QuestionsScreen(),
         // '/fav': (context) => Fav(),
        },
      );
    }
    //print("La PLATAFORMA ES ..... --> "+DataHolder().platformAdmin.isAndroidPlatform().toString());

    return materialAppMobile;
  }
}
