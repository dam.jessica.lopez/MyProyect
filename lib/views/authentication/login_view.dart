import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import '../components_views/custom_button.dart';
import '../components_views/custom_text_field.dart';
import '../style_views/Animations/FadeAnimation.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  String prefix = "+34";
  TextEditingController phoneController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFFF7DDEA),
      body: SafeArea(
        minimum: EdgeInsets.only(top: screenSize.height * 0.05),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Card(
              elevation: 5,
              color: const Color(0xFFE88EED).withOpacity(0.6),
              child: Container(
                width: screenSize.width * 0.9,
                height: screenSize.height * 0.55,
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FadeAnimation(
                          delay: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/cuenta/cerebro.png',
                                width: screenSize.width * 0.3,
                              ),
                              const SizedBox(width: 30),
                              Text(
                                'JBrain',
                                style: TextStyle(
                                  fontSize: screenSize.width * 0.1,
                                  fontWeight: FontWeight.bold,
                                  color: const Color(0xFF311B92),
                                ),
                              ),
                            ],
                          )),
                      SizedBox(height: screenSize.height * 0.03),
                      FadeAnimation(
                        delay: 1,
                        child: Text(
                          'Escribe el teléfono',
                          style: TextStyle(
                            fontSize: screenSize.width * 0.07,
                            color: Colors.grey[800],
                            letterSpacing: 0.5,
                          ),
                        ),
                      ),
                      SizedBox(height: screenSize.height * 0.03),
                      FadeAnimation(
                        delay: 1.2,
                        child: CustomTextField(
                          textInputType: TextInputType.phone,
                          controller: phoneController,
                          hintText: 'Teléfono',
                          icon: Icons.phone,
                          isPassword: false, maxLength: 9,
                        ),
                      ),
                      SizedBox(height: screenSize.height * 0.01),
                      FadeAnimation(
                        delay: 1.6,
                        child: CustomButton(
                          action: () {
                            if (_formKey.currentState!.validate()) {
                              FBAdmin().signInWithPhone(
                                  context, prefix + phoneController.text);
                            }
                          },
                          text: 'ENTRAR',
                          color: const Color(0xFF311B92),
                        ),
                      ),
                      SizedBox(height: screenSize.height * 0.01),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: screenSize.height * 0.02),
            FadeAnimation(
              delay: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Si eres profesional ',
                    style: TextStyle(
                      fontSize: screenSize.width * 0.05,
                      color: Colors.grey[700],
                      letterSpacing: 0.5,
                    ),
                  ),
                ],
              ),
            ),
            FadeAnimation(
              delay: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/loginPro');
                    },
                    child: Text(
                      'Pincha aquí',
                      style: TextStyle(
                        color: Colors.purple[900],
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.5,
                        fontSize: screenSize.width * 0.06,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
