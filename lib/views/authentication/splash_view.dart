import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../controller/FbAdmin.dart';
import '../style_views/hex_color.dart';
import '../../controller/singleton/DataHolder.dart';

class SplashView2 extends StatefulWidget {
  const SplashView2({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SplashView2();
  }
}

class _SplashView2 extends State<SplashView2> {
  FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();
    isUserLogged();
  }

  void isUserLogged() async {
    await Future.delayed(const Duration(seconds: 2));
    //FirebaseAuth.instance.signOut();

    if (DataHolder().auth.currentUser?.uid == null) {
      setState(() {
        print("------->No hay usuario logueado");
        Navigator.of(context).popAndPushNamed("/login");
      });
    } else {
      bool pExist = await FBAdmin().downloadProfile();
      bool pUsuarioExist = await FBAdmin().checkUserExist();
      if (pExist) {
        setState(() {
          //Navigator.of(context).popAndPushNamed("/home");

          Navigator.of(context).popAndPushNamed("/homePro");
        });
      } else if (pUsuarioExist) {
        setState(() {
          Navigator.of(context).popAndPushNamed("/bar");
        });
      } else {
        setState(() {
          //Navigator.of(context).popAndPushNamed("/home");

          Navigator.of(context).popAndPushNamed("/onBoarding");
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: 10000,
            height: 10000,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: const [0.1, 0.4, 0.7, 0.9],
                colors: [
                  HexColor("#4b4293").withOpacity(0.8),
                  HexColor("#4b4293"),
                  HexColor("#08418e"),
                  HexColor("#08418e")
                ],
              ),
              image: DecorationImage(
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    HexColor("#fff").withOpacity(0.2), BlendMode.dstATop),
                image: const AssetImage('assets/images/fondos/fondo.gif'),
              ),
            ),
            child: Column(
              children: [
                 SizedBox(
                  height: MediaQuery.of(context).size.width * 0.5,
                ),
                Image.asset(
                  'assets/images/splash/logo.png',
                  height: 200,
                ),
                 SizedBox(
                  height: MediaQuery.of(context).size.width * 0.5,
                ),
                Image.asset('assets/images/splash/loginLlama.gif', height: 100),
                const Text(
                  "Espera unos segundos...                                                                    ",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
              ],
            )));
  }
}
