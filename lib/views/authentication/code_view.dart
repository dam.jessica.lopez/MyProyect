import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import 'package:my_proyect/views/authentication/auth_profesional/login_view_pro.dart';
import 'package:my_proyect/views/components_views/snack.dart';
import '../components_views/custom_button.dart';
import '../style_views/Animations/FadeAnimation.dart';
import 'package:pinput/pinput.dart';

class CodeView extends StatefulWidget {
  final String verificationId;

  const CodeView({Key? key, required this.verificationId});

  @override
  State<CodeView> createState() => _CodeViewState();
}

class _CodeViewState extends State<CodeView> {
  final _formKey = GlobalKey<FormState>();

  String? code;

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFFF7DDEA),
      body: SafeArea(
        minimum: EdgeInsets.only(top: screenSize.height * 0.05),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                elevation: 5,
                color: const Color(0xFFE88EED).withOpacity(0.8),
                child: Container(
                  width: screenSize.width * 0.9,
                  height: screenSize.height * 0.55,
                  padding: const EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        FadeAnimation(
                            delay: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/images/cuenta/cerebro.png',
                                  width: screenSize.width * 0.3,
                                ),
                                const SizedBox(width: 30),
                                Text(
                                  'JBrain',
                                  style: TextStyle(
                                    fontSize: screenSize.width * 0.1,
                                    fontWeight: FontWeight.bold,
                                    color: const Color(0xFF311B92),
                                  ),
                                ),
                              ],
                            )),
                        const SizedBox(height: 10),
                        FadeAnimation(
                          delay: 1,
                          child: Text(
                            'Escribe el código',
                            style: TextStyle(
                              fontSize: screenSize.width * 0.07,
                              color: Colors.grey[800],
                              letterSpacing: 0.5,
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        FadeAnimation(
                          delay: 1.2,
                          child: Pinput(
                              length: 6,
                              showCursor: true,
                              defaultPinTheme: PinTheme(
                                width: screenSize.width * 0.12,
                                height: screenSize.height * 0.08,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                    color: Colors.deepPurple,
                                    width: 2,
                                  ),
                                ),
                                textStyle: TextStyle(
                                  fontSize: screenSize.width * 0.08,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onCompleted: (value) {
                                setState(() {
                                  code = value;
                                  print(value);
                                });
                              }),
                        ),
                        const SizedBox(height: 10),
                        FadeAnimation(
                          delay: 1.6,
                          child: CustomButton(
                            action: () {
                              if (code != null) {
                                print(code!.toString());
                                verifyCode(context, code!);
                              } else {
                                print(code);
                                showSnackBar(context, "Introduce 6 digitos");
                              }
                            },
                            text: 'ENTRAR',
                            color: const Color(0xFF311B92),
                          ),
                        ),
                        const SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void verifyCode(BuildContext context, String code) async {
    FBAdmin().verifyCode(
        context: context,
        verificationId: widget.verificationId,
        code: code,
        onSuccess: () {
          FBAdmin().checkUserExist().then((value) async {
            if (value) {
              print("Ya existe el usuario");
              Navigator.pushNamedAndRemoveUntil(
                  context, '/bar', (route) => false);
            } else {
              print("Se crea un nuevo usuario");
              FBAdmin().createProfileUser().then((value) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/bar', (route) => false);
              });
            }
          });
        });
  }
}
