import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import '../../components_views/custom_button.dart';
import '../../components_views/input_text.dart';

class OnBoardingView extends StatefulWidget {
  const OnBoardingView({Key? key}) : super(key: key);

  @override
  State<OnBoardingView> createState() => _OnBoardingViewState();
}

class _OnBoardingViewState extends State<OnBoardingView> {

  final _formKey = GlobalKey<FormState>();
  String? imageUrl;

  TextEditingController nombreController = TextEditingController();
  TextEditingController apellidoController = TextEditingController();
  TextEditingController profesionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          title: Text('EDITAR PERFIL',
              textAlign: TextAlign.center, style: TextStyle(fontSize: screenSize.width * 0.07)),
          backgroundColor: Colors.purple.shade700,
          elevation: 10,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).popAndPushNamed('/homePro'),
          )),
      backgroundColor: Colors.deepPurple.shade100,
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          padding: MediaQuery.of(context).size.height > 800
              ? const EdgeInsets.symmetric(vertical: 50, horizontal: 0)
              : const EdgeInsets.symmetric(vertical: 30, horizontal: 0),
          child: Center(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
              ),
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.8,
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    GestureDetector(
                      child: CircleAvatar(
                        radius: MediaQuery.of(context).size.height * 0.1,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          radius: MediaQuery.of(context).size.height * 0.09,
                          backgroundColor: Colors.transparent,
                          backgroundImage: imageUrl != null
                              ? NetworkImage(imageUrl!)
                              : const AssetImage(
                                      'assets/images/cuenta/usuario.png')
                                  as ImageProvider,
                        ),
                      ),
                      onTap: () {
                        FBAdmin().pickUploadImage().then((value) {
                          setState(() {
                            imageUrl =
                                value; // recupero la url del metodo que recupera las fotos del storage.
                          });
                        });
                      },
                    ),
                    InputText(
                      controller: nombreController,
                      titulo: "NOMBRE",
                      icono: const Icon(Icons.person, size: 30, color: Colors.blue),
                    ),
                    InputText(
                        controller: apellidoController,
                        titulo: "APELLIDO",
                        icono: const Icon(Icons.person, size: 30, color: Colors.blue)),
                    InputText(
                        controller: profesionController,
                        titulo: "PROFESIÓN",
                        icono: const Icon(Icons.medical_services, size: 30, color: Colors.blue
                        )),
                    const SizedBox(height: 10),
                    CustomButton(
                      action: () => {
                        if (_formKey.currentState!.validate())
                          {
                            FBAdmin().insertProfile(
                                nombreController.text,
                                apellidoController.text,
                                profesionController.text,
                                imageUrl,
                                context),
                            // Si el formulario es válido, queremos mostrar un Snackbar
                            ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    backgroundColor: Colors.deepPurple,
                                    content: Text('Perfil creado con exito',
                                        style: TextStyle(fontSize: 20),
                                        textAlign: TextAlign.center))),
                            Navigator.of(context).popAndPushNamed("/homePro")
                          }
                      },
                      text: "ACEPTAR",
                      color: Colors.purple.shade700,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
