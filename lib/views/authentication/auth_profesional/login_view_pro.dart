import 'package:flutter/material.dart';
import '../../../controller/FbAdmin.dart';
import '../../components_views/custom_button.dart';
import '../../components_views/custom_text_field.dart';
import '../../components_views/snack.dart';
import '../../style_views/Animations/FadeAnimation.dart';

class LoginViewPro extends StatefulWidget {
  const LoginViewPro({super.key});

  @override
  State<LoginViewPro> createState() => _LoginViewProState();
}

class _LoginViewProState extends State<LoginViewPro> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFFDAEAF6),
      body: Center(
        child: SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          padding: MediaQuery.of(context).padding,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                elevation: 5,
                color: const Color(0xFF97DEFF).withOpacity(0.8),
                child: Container(
                  width: screenSize.width * 0.9,
                  height: screenSize.height * 0.7,
                  padding: const EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        FadeAnimation(
                            delay: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/images/cuenta/cerebro.png',
                                  width: screenSize.width * 0.3,
                                ),
                                const SizedBox(width: 30),
                                Text(
                                  'JBrain',
                                  style: TextStyle(
                                    fontSize: screenSize.width * 0.1,
                                    fontWeight: FontWeight.bold,
                                    color: const Color(0xFF311B92),
                                  ),
                                ),
                              ],
                            )),
                        CustomTextField(
                          controller: emailController,
                          hintText: 'Email',
                          icon: Icons.email_outlined,
                          isPassword: false, maxLength: 30,
                        ),
                        CustomTextField(
                          controller: passwordController,
                          hintText: 'Contraseña',
                          icon: Icons.lock_open_outlined,
                          isPassword: true,
                          suffixIcon: Icons.visibility_off_outlined, maxLength: 30,
                        ),
                        FadeAnimation(
                            delay: 1,
                            child: CustomButton(
                              action: () {
                                if (_formKey.currentState!.validate()) {
                                  // Si el formulario es válido, queremos mostrar un Snackbar
                                  showSnackBar(context, "Procesando datos...");
                                  FBAdmin()
                                      .singIn(emailController.text,
                                          passwordController.text, context)
                                      .then((value) async => {
                                            if (value == null)
                                              {
                                                if (await FBAdmin()
                                                    .downloadProfile())
                                                  {
                                                    Navigator
                                                        .pushReplacementNamed(
                                                            context,
                                                            '/homePro')
                                                  }
                                                else if (await FBAdmin()
                                                    .downloadUserProfile())
                                                  {
                                                    showSnackBar(context,
                                                        "Usuario no autorizado"),
                                                  }
                                                else
                                                  {
                                                    Navigator
                                                        .pushReplacementNamed(
                                                            context,
                                                            '/onBoarding'),
                                                  }
                                              }
                                            //Mensajes de error en el login que se muestran en una snak bar.
                                            else
                                              {
                                                showSnackBar(context, value),
                                              }
                                          });
                                }
                              },
                              text: "ENTRAR",
                              color: const Color(0xFF311B92),
                            )),
                        FadeAnimation(
                          delay: 1,
                          child: TextButton(
                            onPressed: () {
                              FBAdmin().resetPassword(emailController.text, context);
                            },
                            child: Text(
                              '¿Olvidaste tu contraseña?',
                              style: TextStyle(
                                color: const Color(0xFF311B92),
                                fontSize: screenSize.width * 0.04,
                              ),
                            ),
                          ),),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: screenSize.height * 0.01),
              FadeAnimation(
                delay: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("¿No tienes una cuenta? ",
                        style: TextStyle(
                          fontSize: screenSize.width * 0.04,
                          color: Colors.grey,
                          letterSpacing: 0.5,
                        )),
                  ],
                ),
              ),
              FadeAnimation(
                delay: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.popAndPushNamed(context, '/register');
                      },
                      child: Text(" Registrarse",
                          style: TextStyle(
                              color: Colors.purple[900],
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.5,
                              fontSize: screenSize.width * 0.05)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
