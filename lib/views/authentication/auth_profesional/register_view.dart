import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import 'package:my_proyect/views/components_views/custom_text_field.dart';
import 'package:my_proyect/views/components_views/snack.dart';
import '../../components_views/custom_button.dart';
import '../../style_views/Animations/FadeAnimation.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({super.key});

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController repasswordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFFDAEAF6),
      body: Center(
        child: SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          padding: MediaQuery.of(context).padding,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                elevation: 5,
                color:const Color(0xFF97DEFF).withOpacity(0.8),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: MediaQuery.of(context).size.height * 0.75,
                  padding: const EdgeInsets.all(25.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        CustomTextField(
                          controller: emailController,
                          hintText: 'Email',
                          icon: Icons.email_outlined,
                          isPassword: false, maxLength: 30,
                        ),
                        CustomTextField(
                          controller: passwordController,
                          hintText: 'Contraseña',
                          icon: Icons.lock_open_outlined,
                          isPassword: true,
                          suffixIcon: Icons.visibility_off_outlined, maxLength: 30,
                        ),
                        CustomTextField(
                          controller: repasswordController,
                          hintText: 'Contraseña',
                          icon: Icons.lock_open_outlined,
                          isPassword: true,
                          suffixIcon: Icons.visibility_off_outlined, maxLength: 30,
                        ),
                        FadeAnimation(
                            delay: 1,
                            child: CustomButton(
                              action: () {
                                if (_formKey.currentState!.validate()) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                          duration: Duration(seconds: 1),
                                          content: Text('Procesando datos')));
                                  if ((repasswordController.text ==
                                      (passwordController.text))) {
                                    FBAdmin().signUp(emailController.text,
                                        passwordController.text, context);
                                  } else {
                                    showSnackBar(context, "Las contraseñas no coinciden");
                                  }
                                }
                              },
                              text: 'REGISTRAR',
                              color: const Color(0xFF311B92),
                            ))
                      ],
                    ),
                  ),
                ),
              ),

              //End of Center Card
              //Start of outer card
              const SizedBox(
                height: 10,
              ),
              FadeAnimation(
                delay: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("¿Volver atrás? ",
                        style: TextStyle(
                          fontSize: screenSize.width * 0.04,
                          color: Colors.grey,
                        )),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(" Cancelar",
                          style: TextStyle(
                              color: Colors.purple[900],
                              fontWeight: FontWeight.bold,
                              fontSize: screenSize.width * 0.05)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
