import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import 'package:my_proyect/model/PUGame.dart';
import 'package:my_proyect/model/PerfilUser.dart';
import 'package:supercharged/supercharged.dart';

class Fav extends StatefulWidget {
  const Fav({Key? key}) : super(key: key);

  @override
  _FavState createState() => _FavState();
}

class _FavState extends State<Fav> {
  PerfilUser? perfil = DataHolder().perfilUser;
  List<PUGame> sortedGames = [];

  @override
  void initState() {
    super.initState();
    cargarPartidas();
  }

  /*Future<void> cargarPartidas() async {
    await perfil!.getGamesFromFB();

    if (perfil!.games != null) {
      for (final game in perfil!.games!) {
        await game.getLastPartidasFromFB();
      }

      sortedGames = perfil!.games!.toList();
      sortedGames!.sort((a, b) => b.partidas![0].puntos.compareTo(a.partidas![0].puntos));
    }
  }*/

  Future<List<PUGame>> cargarPartidas() async {
  await perfil!.getGamesFromFB();
  if (perfil!.games != null) {
    for (final game in perfil!.games.values) {
      await game.getLastPartidasFromFB();
    }

    List<MapEntry<String, PUGame>> sortedEntries =
        perfil!.games.entries.toList();
    sortedEntries.sort((a, b) {
      final puntosA =
          a.value.partidas != null && a.value.partidas.isNotEmpty
              ? a.value.partidas[0].puntos
              : 0;
      final puntosB =
          b.value.partidas != null && b.value.partidas.isNotEmpty
              ? b.value.partidas[0].puntos
              : 0;

      return puntosB!.compareTo(puntosA!);
    });

    sortedGames =
        sortedEntries.map((entry) => entry.value).toList();
        print(sortedGames.elementAt(0).partidas.toString());
    return sortedGames;
  }
  return []; // Si no hay juegos, devuelve una lista vacía
}


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text("ÚLTIMAS PARTIDAS", style: TextStyle(fontSize: 25)),
        backgroundColor: Colors.purple,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        color: Colors.white,
        child: FutureBuilder<List<PUGame>>(
          future: cargarPartidas(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.purple,
                ), // Indicador de carga
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text('Error al cargar los datos',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: size.width * 0.08,
                          fontWeight: FontWeight.bold)),
                ),
              );
            } else if (perfil == null ||
                sortedGames == null ||
                sortedGames.isEmpty) {
              return Center(
                child: Text(
                  'No tienes partidas aún. ¡Anímate a jugar!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: size.width * 0.08,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: sortedGames.length,
                itemBuilder: (context, index) {
                  final juego = sortedGames.elementAt(index);
                  return Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: Card(
                      color:
                          juego.partidas != null && juego.partidas.isNotEmpty
                              ? Colors.blue
                              : Colors.grey,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: ListTile(
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 8),
                        leading: CircleAvatar(
                          backgroundColor: Colors.white,
                          backgroundImage: NetworkImage(juego.image ?? ""),
                        ),
                        title: Text(
                          juego.name ?? "Sin nombre",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: size.width * 0.08,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (juego.partidas != null &&
                                juego.partidas.isNotEmpty)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${juego.partidas[0].fecha!.toDate().day}/${juego.partidas[0].fecha!.toDate().month}/${juego.partidas[0].fecha!.toDate().year}",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: size.width * 0.06,
                                    ),
                                  ),
                                ],
                              )
                            else
                              const Text(
                                "No hay partidas",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                          ],
                        ),
                        trailing:
                            juego.partidas != null && juego.partidas.isNotEmpty
                                ? Text(
                                    juego.partidas[0].puntos.toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: size.width * 0.07,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                : Text(
                                    "0",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: size.width * 0.08,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                      ),
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}
