import 'package:flutter/material.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import '../../controller/FbAdmin.dart';
import '../../controller/admin_games.dart';
import '../../model/Game.dart';
import '../../model/GameItem.dart';
import '../../model/PerfilUser.dart';
import '../games_views/Calculo/Levels/levels_calculo_view.dart';
import '../games_views/Frase/levels_phrases_view.dart';
import '../games_views/Memory/Levels/levels_memory_view.dart';
import '../games_views/PhotoMemory/Levels/levels_photo_memory_view.dart';
import '../games_views/Quiz/Levels/levels_quiz_view.dart';
import '../games_views/Simon/Levels/levels_simon_view.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});


  @override
  State<StatefulWidget> createState() {
    return _HomeViewState();
  }


}

class _HomeViewState extends State<HomeView> {
  final AdminGames ag = AdminGames();
  final DataHolder dataHolder = DataHolder();

  PerfilUser? perfil = PerfilUser();

  @override
  void initState() {
    super.initState();
    getGamesList();
    updateNameUser();
    
  }

  Future<void> cargarJuegos() async {
    await perfil!.getGamesFromFB();
    print(perfil!.games.values.toList());
  }

  updateNameUser() {
    FBAdmin().getProfileUser().then((value) {
      setState(() {
        if (value != null) {
          perfil = value;
          cargarJuegos();
          DataHolder().perfilUser = perfil!;
        } else {
          perfil = null;
          DataHolder().perfilUser = perfil!;
        }
      });
    });
  }

  void getGamesList() async {
    final docRef = DataHolder().db.collection("games").withConverter(
        fromFirestore: Game.fromFirestore,
        toFirestore: (Game game, _) => game.toFirestore());

    final docsSnap = await docRef.get();

    setState(() {
      dataHolder.games.clear();
      for (int i = 0; i < docsSnap.docs.length; i++) {
        dataHolder.games.add(docsSnap.docs[i].data());
      }
    });
  }

 

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('INICIO',
              textAlign: TextAlign.center, style: TextStyle(fontSize: 30)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.purple,
          elevation: 10,
        ),
        backgroundColor: Colors.lightBlue,
        body: GridView.builder(
            padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.005),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 2,
            ),
            itemCount: dataHolder.games.length,
            itemBuilder: (BuildContext context, int index) {
              return GameItem(
                  ImgUrl: DataHolder().games[index].image!,
                  Name: DataHolder().games[index].name!,
                  onShortClick: (index) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ag.pages[index],
                      ),
                    );
                  },
                  index: index);
            }),
      ),
    );
    //),
    // );
  }
}
