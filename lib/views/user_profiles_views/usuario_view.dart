import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import 'package:my_proyect/views/components_views/custom_row_profile.dart';
import 'package:my_proyect/views/user_profiles_views/fav.dart';
import '../../model/PerfilUser.dart';
import '../authentication/login_view.dart';
import '../style_views/hex_color.dart';

class UsuarioView extends StatefulWidget {
  const UsuarioView({super.key});

  @override
  State<UsuarioView> createState() => _UsuarioViewState();
}

class _UsuarioViewState extends State<UsuarioView> {
  PerfilUser? perfil = DataHolder().perfilUser;
  String? phoneNumber;
  String? name;

  @override
  void initState() {
    super.initState();
    perfil = DataHolder().perfilUser;
    if (perfil != null) {
      phoneNumber = perfil!.phone;
      setState(() {
        name = perfil!.name;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screensize = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.lightBlue.shade700,
            elevation: 0.0,
            automaticallyImplyLeading: false,
            
            ),
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: screensize.width * 0.04,
                        vertical: screensize.height * 0.04),
                    width: screensize.width,
                    height: screensize.height * 0.45,
                    decoration: BoxDecoration(
                      boxShadow: const [
                        BoxShadow(color: Colors.black, blurRadius: 10)
                      ],
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.elliptical(500, 300),
                      ),
                      gradient: LinearGradient(colors: [
                        Colors.lightBlue.shade700,
                        Colors.lightBlue.shade600,
                        Colors.lightBlue.shade500,
                        Colors.lightBlue.shade400,
                        Colors.lightBlue.shade300,
                        Colors.lightBlue.shade200,
                      ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: const [
                            0.1,
                            0.3,
                            0.5,
                            0.7,
                            0.9,
                            1
                          ]
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                             Text("Hola ",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: screensize.width*0.08,
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center),
                            SizedBox(width: screensize.width * 0.05),
                            Text("$name" == "" ? "Jugador" : "$name",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: screensize.width * 0.1,
                                    fontFamily: "Courier New",
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center),
                          ],
                        ),
                         Text(
                          "¡Vamos a divertirnos!",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: screensize.width*0.09,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: screensize.height * 0.08,
                          backgroundImage: const AssetImage(
                              "assets/images/cuenta/cerebro.png"),
                        ),
                      ],
                    )),
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Fav()));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: screensize.width * 0.05,
                          vertical: screensize.height * 0.02),
                      width: screensize.width * 0.6,
                      height: screensize.height * 0.1,
                      decoration: BoxDecoration(
                        color: Colors.purple[100],
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: const Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.favorite,
                            color: Colors.deepPurple,
                            size: 30,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Partidas",
                            style: TextStyle(
                                color: Colors.deepPurple,
                                fontSize: screensize.width * 0.07,
                                fontFamily: "Arial",
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      FBAdmin().singOut(context).then((value) => {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const LoginView()),
                                (route) => false)
                          });
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: screensize.width * 0.05,
                          vertical: screensize.height * 0.02),
                      width: screensize.width * 0.6,
                      height: screensize.height * 0.1,
                      decoration: BoxDecoration(
                        color: Colors.purple[100],
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: const Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.exit_to_app,
                            color: Colors.deepPurple,
                            size: 30,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Salir",
                            style: TextStyle(
                                color: Colors.deepPurple,
                                fontSize: screensize.width * 0.07,
                                fontFamily: "Arial",
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
