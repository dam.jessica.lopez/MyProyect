import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String text) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.purple.shade700,
      content: Text(text, style: const TextStyle(fontSize: 20)),
      duration: const Duration(seconds: 2),
    ),
  );
}