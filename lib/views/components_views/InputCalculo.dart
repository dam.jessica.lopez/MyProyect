import 'package:flutter/material.dart';

class InputCalculo extends StatelessWidget {
  final String titulo;
  final String ayuda;
  final Icon icono;

  InputCalculo({
    Key? key,
    this.titulo = "",
    this.ayuda = "",
    required this.icono,
  }) : super(key: key);

  final TextEditingController myController = TextEditingController();

  String getText() {
    return myController.text;
  }

  void setText(String text) {
    myController.text = text;
  }

  
  @override
  Widget build(BuildContext context) {
    return TextField(
      style: const TextStyle(fontSize: 14),
      textAlignVertical: TextAlignVertical.center,
      maxLines: 1,
      keyboardType: TextInputType.number,
      controller: myController,
      cursorColor: Colors.deepPurple,
      cursorHeight: 20,
      maxLength: 3,
      decoration: InputDecoration(
        //hintText: 'Introduce un número',
        labelText: titulo,
        labelStyle: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(
            color: Colors.grey.withOpacity(0.5),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}
