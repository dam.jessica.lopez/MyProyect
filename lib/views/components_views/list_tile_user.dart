import 'package:flutter/material.dart';

//Formato de los list del menu de usuario.
class ListTileUser extends StatefulWidget {
  final String? text;
  final IconData icon;
  final Function() onTap;
  final Function() onLongPress;
  final Function() trailing;

  const ListTileUser(
      {Key? key, required this.text, required this.icon, required this.onTap, required this.onLongPress, required this.trailing})
      : super(key: key);
      

  @override
  State<ListTileUser> createState() => _ListTileUserState();
}

class _ListTileUserState extends State<ListTileUser> {

  double size(double size) {
      double factor = MediaQuery.of(context).size.height / 700;
      return size * factor;
    }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: size(5), horizontal: size(10)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
      ),
      
      height: MediaQuery.of(context).size.height * 0.1,
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: size(20), vertical: size(7)),
        leading: Icon(widget.icon, color: Colors.purple, size: size(30)),
        title: Text(widget.text!, 
        style: TextStyle( fontSize: size(23), 
        color: Colors.deepPurple.shade900,
        fontWeight: FontWeight.w400, letterSpacing: 1,
        fontFamily: 'Arial'
        )),
        onTap: widget.onTap,
        onLongPress: widget.onLongPress,
        trailing: IconButton(
          onPressed: widget.trailing,
          icon: Icon(Icons.arrow_forward, color: Colors.blue, size: size(40)),
        ),
      ),
    );
  }
}