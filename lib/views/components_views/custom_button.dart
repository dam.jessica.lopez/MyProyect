import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final VoidCallback action;
  final String text;
  final Color color;
  const CustomButton(
      {super.key,
      required this.action,
      required this.text,
      required this.color});

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  

  @override
  Widget build(BuildContext context) {

Size screenSize = MediaQuery.of(context).size;
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: widget.action,
        style: ElevatedButton.styleFrom(
          
          backgroundColor: widget.color,
          foregroundColor: Colors.white,
          minimumSize: screenSize.height > 800
              ?  Size(screenSize.height* 0.4, screenSize.height * 0.1)
              :  Size(screenSize.height* 0.2, screenSize.height* 0.075),
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        child:
            Text(widget.text, style: TextStyle(fontSize: screenSize.width * 0.10)),
      ),
    );
  }
}
