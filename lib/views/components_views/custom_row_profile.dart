import 'package:flutter/material.dart';

class CustomRowProfile extends StatefulWidget {

  final VoidCallback action; 
  final Color color; 
  final Icon icon;
  final String text;

  const CustomRowProfile({super.key, required this.action, required this.color, required this.icon, required this.text});
  

  @override
  State<CustomRowProfile> createState() => _CustomRowProfileState();
  
}

class _CustomRowProfileState extends State<CustomRowProfile> {

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          RawMaterialButton(
            onPressed: widget.action,
            elevation: 4.0,
            fillColor: widget.color, 
            padding: const EdgeInsets.all(20.0),
            shape: const CircleBorder(
              side: BorderSide(color: Colors.white, width: 3.0)
            ),
            child: widget.icon,
          ),
          Container(
            padding: EdgeInsets.only(left: screenSize.width * 0.05),
           
            child: Text(
              widget.text,
              style: TextStyle(
                fontSize: screenSize.width * 0.08, 
                color: Colors.deepPurple,
                fontWeight: FontWeight.bold,
                letterSpacing: screenSize.width * 0.01,
                
                  

              ),
            ),
          )
        ],
      
    );
  }
}
