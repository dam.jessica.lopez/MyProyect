import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final IconData icon;
  final String hintText;
  bool isPassword;
  IconData? suffixIcon;
  TextInputType? textInputType;
  int maxLength;

  CustomTextField({
    Key? key,
    required this.controller,
    required this.icon,
    required this.hintText,
    required this.isPassword,
    this.suffixIcon,
    this.textInputType,
    required this.maxLength,
  }) : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return TextFormField(
      keyboardType: widget.textInputType,
      validator: (value) {
        if (value!.isEmpty) {
          return 'Por favor ingrese un valor';
        } else {
          return null;
        }
      },

      controller: widget.controller,
      obscureText: widget.isPassword,
      style: TextStyle(
        fontSize: screenSize.width * 0.07,
        color: Colors.black,
      ),
      maxLength: widget.maxLength,
      decoration: InputDecoration(
        fillColor: Colors.white.withOpacity(0.9),
        filled: true,
        prefixIcon: Padding(
          padding: EdgeInsetsDirectional.only(start: screenSize.width * 0.03),
          child: Icon(
            widget.icon,
            size: screenSize.width * 0.06,
            color: Colors.grey[700],
          ),
        ),
        suffixIcon: Padding(
          padding: EdgeInsetsDirectional.only(end: screenSize.width * 0.03),
          child: widget.suffixIcon != null
              ? IconButton(
            onPressed: () {
              setState(() {
                widget.isPassword = !widget.isPassword;
              });
            },
            icon: Icon(
              widget.suffixIcon,
              size: screenSize.width * 0.06,
              color: Colors.grey[700],
            ),
          )
              : null,
        ),
        
        hintText: widget.hintText,
        hintStyle: TextStyle(
          fontSize: screenSize.width * 0.07,
          color: Colors.grey[700],
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.0),
          borderSide: const BorderSide(
            color: Colors.deepPurple,
          )
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.0),
          borderSide: const BorderSide(
            color: Colors.deepPurple,
            width: 4.0,
          )
        ),
      ),
    );
  }
}
