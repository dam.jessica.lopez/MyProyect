import 'package:flutter/material.dart';
class InputText extends StatefulWidget {

  final String titulo;
  final String ayuda;
  final bool blIsPsswd;
  final Icon icono;
  final controller; 
  TextInputType? textInputType;

  IconButton? iconButton;

  InputText({
    Key? key,
    this.titulo = "",
    this.ayuda = "",
    this.blIsPsswd = false,
    this.iconButton,
    this.controller,
    this.textInputType,
    required this.icono,
  }) : super(key: key);


  @override
  State<InputText> createState() => _InputTextState();
}

class _InputTextState extends State<InputText> {
  

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return TextFormField(
      validator: (value) {
        if (value!.isEmpty) {
          return 'Por favor ingrese un valor';
        } else {
          return null;
        }
      },
      keyboardType: widget.textInputType,
      style: TextStyle(
          fontSize: screenSize.width * 0.06, color: Colors.black, height: 1.5),
      obscureText: widget.blIsPsswd,
      controller: widget.controller,
      cursorColor: Colors.deepPurple,
      cursorHeight: 30,
      maxLength: 30,
      enableSuggestions: !widget.blIsPsswd,
      autocorrect: !widget.blIsPsswd,
      decoration: InputDecoration(
        contentPadding: screenSize.height > 700
            ? const EdgeInsets.symmetric(vertical: 20, horizontal: 10)
            : const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        //border: const OutlineInputBorder(),
        prefixIcon: widget.icono,
        suffixIcon: widget.iconButton,
        labelText:widget.titulo,
        labelStyle: TextStyle(
          color: Colors.blue,
          fontSize: screenSize.width * 0.06,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.italic,
        ),
        helperText: widget.ayuda,
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12.0),
            borderSide: const BorderSide(
              color: Colors.deepPurple,
            )
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12.0),
            borderSide: const BorderSide(
              color: Colors.deepPurple,
              width: 2.0,
            )
        ),

      ),
    );
  }
}
