import 'package:flutter/material.dart';

class LevelButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color customColor;

  const LevelButton({super.key, required this.text, required this.customColor, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: screenSize.width * 0.1, horizontal: screenSize.width * 0.08),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: customColor, // background
            shadowColor: Colors.black, //
            elevation: 7.0, // foreground
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          ),
          onPressed: onPressed,
          child: Text(text, style: TextStyle(fontSize: screenSize.width >= 800 ? 100 : 70)),
        ),
      ),
    );
  }
}
