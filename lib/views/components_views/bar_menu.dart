import 'package:flutter/material.dart';

import '../user_profiles_views/home_view.dart';
import '../user_profiles_views/usuario_view.dart';


class BarMenu extends StatefulWidget {
  const BarMenu({super.key});

  @override
  State<BarMenu> createState() => _BarMenu();
}

class _BarMenu extends State<BarMenu> {

  int _paginaIndex=0;

  List<Widget> _paginas=[
    //SplashView(),
    HomeView(),
    UsuarioView(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body: _paginas[_paginaIndex],
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: Colors.purple.shade900,
            selectedItemColor: Colors.blueAccent.shade200,
            unselectedItemColor: Colors.white12.withOpacity(.60),
            mouseCursor: SystemMouseCursors.click,
            iconSize: 50,
            currentIndex: _paginaIndex,
            onTap: (index){
              setState((){
                _paginaIndex= index;
              });
            },
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home),label: "Juegos",),
              BottomNavigationBarItem(icon: Icon(Icons.person),label: "Perfil")
            ],
          ),
    );
  }
}

