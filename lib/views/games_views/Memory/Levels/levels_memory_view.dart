import 'package:flutter/material.dart';
import '../../../components_views/bar_menu.dart';
import '../../../components_views/level_button.dart';
import '../../../style_views/hex_color.dart';
import 'Level1/Memory.dart';
import 'Level2/Memory2.dart';
import 'Level3/Memory3.dart';

class NivelesMemoryView extends StatefulWidget{
  const NivelesMemoryView({super.key});

  @override
  State<StatefulWidget> createState() {
    return _NivelesMemoryView();
  }
}


class _NivelesMemoryView extends State<NivelesMemoryView>{
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('MEMORIZA',textAlign: TextAlign.center,style: TextStyle(fontSize: 30)),
          backgroundColor: Colors.purple,
          elevation: 30,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const BarMenu(),
                ),
              );
            },
        ),
          actions: [
            IconButton(
              icon: const Icon(Icons.info),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Información'),
                      content: const Text('Este juego esta basado en emparejar.\n\nConsta de una pantalla donde se muestra varias fotos dadas la vuelta.\nSe llevará un conteo de los intentos en los que se ha completado el juego. \n\nLa puntuación será visible durante todo el juego.'),
                      actions: [
                        TextButton(
                          child: const  Text('Cerrar'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          ],
        ),
        backgroundColor: Colors.lightBlue,
        body: SafeArea(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const SizedBox(
                height: 60,
              ),
              LevelButton(text: "NIVEL 1", customColor: Colors.purple.shade300, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Memory(),
                  ),
                );
              }),
    
              LevelButton(text: "NIVEL 2", customColor: Colors.purple, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Memory2(),
                  ),
                );
              }),
    
              LevelButton(text: "NIVEL 3", customColor: Colors.purple.shade800, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Memory3(),
                  ),
                );
              }),
    
            ],
          ),
        ),
    
      ),
    );
  }
}