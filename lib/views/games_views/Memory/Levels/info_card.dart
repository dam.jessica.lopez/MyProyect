import 'package:flutter/material.dart';

Widget infoCard(String title, String info, BuildContext context) {
  Size size = MediaQuery.of(context).size;
  return Expanded(
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.03, vertical: size.height * 0.02),
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 26.0),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: size.width * 0.07,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            info,
            style: TextStyle(fontSize: size.width * 0.07, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    ),
  );
}