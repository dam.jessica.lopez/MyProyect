import 'dart:math';
import 'package:flutter/material.dart';

class Game {

  final Color hiddenCard = Colors.red;
  List<Color>? gameColors;
  List<String>? gameImg;
  List<Color> cards = [
    Colors.green,
    Colors.yellow,
    Colors.yellow,
    Colors.green,
    Colors.blue,
    Colors.blue
  ];

  static List<String> images = [
    "assets/images/memory/Alpargatas.jpg",
    "assets/images/memory/Manopla.jpg",
    "assets/images/memory/Taza.jpg",
    "assets/images/memory/Cazos.jpg",
  ];

   static List<int> pos = [];


  static List<int> generateRandomNumbers() {
    pos = [];

    while (pos.length < 4) {
      int numero = Random().nextInt(4);
      if (!pos.contains(numero)) {
        pos.add(numero);
      }
    }

    return pos;
  }



  final String hiddenCardpath = "assets/images/memory/hidden.png";
  List<String> cardsList = [];

  final int cardCount = 8;
  List<Map<int, String>> matchCheck = [];

  void initGame() {
    pos = generateRandomNumbers();
    cardsList = [
      images[pos[0]],images[pos[1]],
      images[pos[0]],images[pos[2]],
      images[pos[3]],images[pos[1]],
      images[pos[3]],images[pos[2]],

    ];
    gameColors = List.generate(cardCount, (index) => hiddenCard);
    gameImg = List.generate(cardCount, (index) => hiddenCardpath);
  }

}