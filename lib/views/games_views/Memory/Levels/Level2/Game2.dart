import 'dart:math';

import 'package:flutter/material.dart';

class Game2 {
  final Color hiddenCard = Colors.red;
  List<Color>? gameColors;
  List<String>? gameImg;
  List<Color> cards = [
    Colors.green,
    Colors.yellow,
    Colors.yellow,
    Colors.green,
    Colors.blue,
    Colors.blue,
    Colors.purple,
    Colors.purple
  ];



  static List<String> images = [
    "assets/images/memory/Alpargatas.jpg",
    "assets/images/memory/Manopla.jpg",
    "assets/images/memory/Taza.jpg",
    "assets/images/memory/Cazos.jpg",
    "assets/images/memory/Hamburguesa.jpg",
    "assets/images/memory/Donut.jpg",
  ];

  static List<int> pos = [];

  static List<int> generateRandomNumbers() {
    pos = [];

    while (pos.length < 6) {
      int numero = Random().nextInt(6);
      if (!pos.contains(numero)) {
        pos.add(numero);
      }
    }
    return pos;
  }


  final String hiddenCardpath = "assets/images/memory/hidden.png";
  List<String> cardsList = [];
  final int cardCount = 12;
  List<Map<int, String>> matchCheck = [];

  //methods
  void initGame() {
    pos = generateRandomNumbers();
    cardsList = [
      images[pos[0]],images[pos[1]],images[pos[2]],images[pos[3]],
      images[pos[0]],images[pos[4]],images[pos[5]],images[pos[3]],
      images[pos[2]],images[pos[1]],images[pos[5]],images[pos[4]],
    ];
    gameColors = List.generate(cardCount, (index) => hiddenCard);
    gameImg = List.generate(cardCount, (index) => hiddenCardpath);
  }
}