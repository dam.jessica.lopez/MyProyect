import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../../../../../controller/singleton/DataHolder.dart';
import '../../../../../model/PUGame.dart';
import '../../../../../model/Partida.dart';
import '../../../../../model/PerfilUser.dart';
import '../../pop_menu.dart';
import '../levels_memory_view.dart';
import '../info_card.dart';
import 'Game2.dart';

class Memory2 extends StatefulWidget {
  const Memory2({Key? key}) : super(key: key);

  @override
  _MemoryState2 createState() => _MemoryState2();
}

class _MemoryState2 extends State<Memory2> {
  TextStyle whiteText = const TextStyle(color: Colors.white);
  bool hideTest = false;
  Game2 _game = Game2();

  final level = 2;
  final String key = "MEMORIZA";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;


  int tries = 0;
  int score = 0;

  @override
  void initState() {
    super.initState();
    _game.initGame();
     getKeyGame(); 
  }

  void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      print('Clave:$key ${pUG.uid}');
      
    } else {
      print('No existe la clave');
    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = score;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'BUSCA LA PAREJA',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30),
          ),
          backgroundColor: Colors.purple,
          elevation: 10,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const NivelesMemoryView(),
                ),
              );
            },
          ),
        ),
        backgroundColor: Colors.purple[100],
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  infoCard("Intentos", "$tries", context),
                  infoCard("Puntos", "$score", context),
                ],
              ),
              SizedBox(
                  height: MediaQuery.of(context).size.width,
                  width: MediaQuery.of(context).size.width,
                  child: GridView.builder(
                      itemCount: _game.gameImg!.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                        crossAxisSpacing: 10.0,
                        mainAxisSpacing: 10.0,
                      ),
                      padding: const EdgeInsets.all(10.0),
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            print(_game.matchCheck);
                            setState(() {
                              //aumenta los clicks
                              tries++;
                              _game.gameImg![index] = _game.cardsList[index];
                              _game.matchCheck
                                  .add({index: _game.cardsList[index]});
                              print(_game.matchCheck.first);
                            });
                            if (_game.matchCheck.length == 2) {
                              if (_game.matchCheck[0].values.first ==
                                  _game.matchCheck[1].values.first) {
                                print("true");
                                //aumenta los puntos
                                score += 100;
                                if (score == 600) {
                                  popMenu(context, "¡FELICIDADES!", "¿QUIERES VOLVER A NIVELES?", saveMatch);
                                }
                                _game.matchCheck.clear();
                              } else {
                                print("false");

                                Future.delayed(
                                    const Duration(milliseconds: 500), () {
                                  print(_game.gameColors);
                                  setState(() {
                                    _game.gameImg![_game.matchCheck[0].keys
                                        .first] = _game.hiddenCardpath;
                                    _game.gameImg![_game.matchCheck[1].keys
                                        .first] = _game.hiddenCardpath;
                                    _game.matchCheck.clear();
                                  });
                                });
                              }
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              color: Colors.cyan,
                              borderRadius: BorderRadius.circular(8.0),
                              image: DecorationImage(
                                image: AssetImage(_game.gameImg![index]),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        );
                      }))
            ],
          ),
        ),
      ),
    );
  }
}
