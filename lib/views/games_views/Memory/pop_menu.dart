import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/Memory/Levels/levels_memory_view.dart';

Future<void> popMenu(BuildContext context, 
String title, String content, VoidCallback onPressed){
  return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: 27)),
          content: Text(
            content, textAlign: TextAlign.center, style: TextStyle(fontSize: 23)
          ),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              onPressed: (){
                onPressed();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const NivelesMemoryView(),
                    ));
              },
              child: const Text('SI', style: TextStyle(fontSize: 25)),
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              onPressed: (){
                 onPressed();
                Navigator.popAndPushNamed(context, '/bar');
              },
              child: const Text('NO', style: TextStyle(fontSize: 25)),
            ),
          ],
        );
      });
}
