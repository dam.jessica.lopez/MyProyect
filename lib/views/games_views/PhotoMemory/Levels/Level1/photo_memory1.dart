import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import '../../../../../controller/singleton/DataHolder.dart';
import '../../../../../model/PUGame.dart';
import '../../../../../model/Partida.dart';
import '../../../../../model/PerfilUser.dart';
import '../../alert_dialog.dart';
import '../levels_photo_memory_view.dart';

class PhotoMemory1 extends StatefulWidget {
  const PhotoMemory1({super.key});

  @override
  _PhotoMemory1 createState() => _PhotoMemory1();
}

class _PhotoMemory1 extends State<PhotoMemory1> {
  late List<Map<String, dynamic>> options;
  late Map<String, dynamic> pSelec;
  List<Map<String, dynamic>> pJson = [];
  bool _visible = true;

  late int points = 0;
  late int index = 0;

  final level = 1;
  final String key = "RECUERDA";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;


  @override
  void initState() {
    super.initState();
    getJson();
    getKeyGame();
  }

   void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      //print('Clave:' + key + ' ' + pUG.uid);

    } else {
      //print('No existe la clave');
    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = points;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }

  void getJson() {
    pJson = [
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "¿LA NIÑA TENIA EL PELO..?",
        "respuestas": [
          {"respuesta": "LARGO", "esCorrecta": false},
          {"respuesta": "CORTO", "esCorrecta": true},
        ]
      },
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "¿CUÁNTOS NIÑOS HABÍA?",
        "respuestas": [
          {"respuesta": "3", "esCorrecta": false},
          {"respuesta": "1", "esCorrecta": false},
          {"respuesta": "2", "esCorrecta": true},
        ]
      },
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "¿EL COLOR ERA LA CAJA ES?",
        "respuestas": [
          {"respuesta": "VERDE", "esCorrecta": true},
          {"respuesta": "ROSA", "esCorrecta": false},
          {"respuesta": "AZUL", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "¿DE QUÉ COLOR ERA LA CAMISETA DE LA NIÑA?",
        "respuestas": [
          {"respuesta": "VERDE", "esCorrecta": false},
          {"respuesta": "NARANJA", "esCorrecta": true},
          {"respuesta": "AZUL", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "LA NIÑA TIENE EN LA MANO...",
        "respuestas": [
          {"respuesta": "PELOTA", "esCorrecta": false},
          {"respuesta": "GLOBO TERRÁQUEO", "esCorrecta": true},
          {"respuesta": "PELUCHE", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "ABAJO A LA IZQUIERDA HAY...",
        "respuestas": [
          {"respuesta": "PELOTA", "esCorrecta": true},
          {"respuesta": "RADIO", "esCorrecta": false},
          {"respuesta": "PLANTA", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "ENCIMA DEL ESCRITORIO HAY...",
        "respuestas": [
          {"respuesta": "COMETA", "esCorrecta": false},
          {"respuesta": "LÁMPARA", "esCorrecta": true},
          {"respuesta": "CASITA", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/ninios.jpg",
        "pregunta": "¿HAY UNA PLANTA?",
        "respuestas": [
          {"respuesta": "SI", "esCorrecta": true},
          {"respuesta": "NO", "esCorrecta": false},
        ]
      },
    ];

    options = List<Map<String, dynamic>>.from(pJson);
    changeQuestion();
  }

  void changeQuestion() {
    if (options.isNotEmpty) {

      final random = Random();
      index = random.nextInt(options.length);
      pSelec = options[index];
      options.removeAt(index);
      //print(index);
      //print(options.length);

      setState(() {});

    } else {
       showEndGameMemory(context, "¡FELICIDADES!", " Puntuación total: $points \n ¿Quieres volver a los niveles?",saveMatch);
     setState(() {

     });
      //options.removeAt(index);

    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    final imagePath = pSelec['imagen'];
    final question = pSelec['pregunta'];
    final answers = pSelec['respuestas'] as List<dynamic>;

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('MEMORIZA LA FOTO',
              style: TextStyle(fontSize: 30),
              textAlign: TextAlign.center),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const NivelesPhotoMemoryView(),
                ),
              );
            },
          ),
        ),
        backgroundColor: Colors.pink[100],
        body: SafeArea(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedOpacity(
                  opacity: _visible ? 1.0 : 0.0,
                  duration: const Duration(milliseconds: 5000),
                  child: Visibility(
                    visible: _visible,
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: size.height * 0.05),
                          width: size.width,
                          height: size.height * 0.4,
                          decoration: BoxDecoration(
                            color: Colors.pink[200],
                            border: Border.all(width: 3),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(imagePath.toString()),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: size.width * 0.6,
                          height: size.height * 0.1,
                          child: FloatingActionButton.extended(
                            heroTag: "btn1",
                            backgroundColor: const Color(0xff03dac6),
                            foregroundColor: Colors.black,
                            onPressed: () {
                              setState(() {
                                _visible = !_visible;
                              });
                            },
                            tooltip: 'JUGAR',
                            label: Text(
                              "JUGAR",
                              style: TextStyle(fontSize: size.width * 0.08),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                AnimatedOpacity(
                  opacity: _visible ? 0.0 : 1.0,
                  duration: const Duration(milliseconds: 5000),
                  child: Visibility(
                    visible: !_visible,
                    child: Column(
                      children: [
                        Text(
                          question,
                          style: TextStyle(
                            fontSize: size.width * 0.07,
                            color: Colors.purple,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 20),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: answers.length,
                          itemBuilder: (context, index) {
                            final respuesta = answers[index];
                            final respuestaText = respuesta['respuesta'];
                            final esCorrecta = respuesta['esCorrecta'];
                            return Container(
                              margin: EdgeInsets.only(
                                  bottom: size.height * 0.02,
                                  left: size.width * 0.1,
                                  right: size.width * 0.1),
                              child: ListTile(
                                shape: RoundedRectangleBorder(
                                  side: const BorderSide(
                                      width: 2, color: Colors.purple),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                title: Text(
                                  respuestaText,
                                  style: TextStyle(fontSize: size.width * 0.07),
                                  textAlign: TextAlign.center,
                                ),
                                onTap: () {
                                  setState(() {
                                    if (esCorrecta) {
                                      points += 100;
                                      alertRespuestas(
                                        context,
                                        "Respuesta Correcta",
                                        "¡Muy bien!",
                                        Colors.lightGreenAccent,

                                      );
                                      changeQuestion();
                                    } else {
                                      alertRespuestas(
                                        context,
                                        "Respuesta Incorrecta",
                                        "¡Suerte la próxima!",
                                        Colors.white,

                                      );
                                      changeQuestion();
                                    }
                                  });
                                },
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
