import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/PhotoMemory/Levels/levels_photo_memory_view.dart';
import 'dart:math';

import '../../../../../controller/singleton/DataHolder.dart';
import '../../../../../model/PUGame.dart';
import '../../../../../model/Partida.dart';
import '../../../../../model/PerfilUser.dart';
import '../../alert_dialog.dart';

class PhotoMemory3 extends StatefulWidget {
  const PhotoMemory3({super.key});

  @override
  _PhotoMemory3 createState() => _PhotoMemory3();
}

class _PhotoMemory3 extends State<PhotoMemory3> {
  late List<Map<String, dynamic>> options;
  late Map<String, dynamic> pSelec;
  List<Map<String, dynamic>> pJson = [];
  bool _visible = true;
  late int points=0;
  late int index = 0;

  final level = 1;
  final String key = "RECUERDA";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;


  @override
  void initState() {
    super.initState();
    getJson();
    getKeyGame();
  }
   void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      //print('Clave:' + key + ' ' + pUG.uid);

    } else {
      //print('No existe la clave');
    }
  }
  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = points;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }

  void getJson() {
    pJson =  [
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "¿QUÉ HABÍA DETRÁS?",
        "respuestas": [
          {"respuesta": "RIO", "esCorrecta": false},
          {"respuesta": "REBAÑO", "esCorrecta": true},
          {"respuesta": "CAMINO", "esCorrecta": false},

        ]
      },
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "EL COLOR DE LA CAMISETA ES...",
        "respuestas": [
          {"respuesta": "VERDE", "esCorrecta": false},
          {"respuesta": "ROSA", "esCorrecta": false},
          {"respuesta": "AZUL", "esCorrecta": true},

        ]
      },
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "EL COLOR DEL CIELO ES...",
        "respuestas": [
          {"respuesta": "NARANJA", "esCorrecta": true},
          {"respuesta": "AZUL", "esCorrecta": false},
          {"respuesta": "GRIS", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "¿SOBRE QUÉ MONTA?",
        "respuestas": [
          {"respuesta": "BICI", "esCorrecta": true},
          {"respuesta": "COCHE", "esCorrecta": false},
          {"respuesta": "PATINES", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "¿QUE ES LO QUE LLEVA?",
        "respuestas": [
          {"respuesta": "SOMBRILLA", "esCorrecta": false},
          {"respuesta": "PARAGÜAS", "esCorrecta": false},
          {"respuesta": "MOCHILA", "esCorrecta": true},
        ]
      },
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "DETRÁS A LA DERECHA HAY...",
        "respuestas": [
          {"respuesta": "ESPANTAPÁJAROS", "esCorrecta": true},
          {"respuesta": "NIÑO", "esCorrecta": false},
          {"respuesta": "ABUELITO", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "¿CUÁNTOS PÁJAROS HAY?",
        "respuestas": [
          {"respuesta": "6", "esCorrecta": false},
          {"respuesta": "4", "esCorrecta": true},
          {"respuesta": "3", "esCorrecta": false},
        ]
      },
      {
        "imagen": "assets/images/memory2/bici.jpg",
        "pregunta": "¿CUÁNTAS AMAPOLAS HAY?",
        "respuestas": [
          {"respuesta": "5", "esCorrecta": true},
          {"respuesta": "7", "esCorrecta": false},
          {"respuesta": "3", "esCorrecta": false},
        ]
      },
    ];


    options = List<Map<String, dynamic>>.from(pJson);
    changeQuestion();
    
  }

  void changeQuestion() {
    if (options.isNotEmpty) {

      final random = Random();
      index = random.nextInt(options.length);
      pSelec = options[index];
      options.removeAt(index);
      //print(index);
      //print(options.length);

      setState(() {});

    } else {
       showEndGameMemory(context, "¡FELICIDADES!",
          "Puntuación total: $points \n ¿Quieres volver a los niveles?",saveMatch);
     setState(() {

     });
      //options.removeAt(index);

    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    final imagePath = pSelec['imagen'];
    final question = pSelec['pregunta'];
    final answers = pSelec['respuestas'] as List<dynamic>;

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('MEMORIZA LA FOTO',style: TextStyle(fontSize: 30),textAlign: TextAlign.center),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const NivelesPhotoMemoryView(),
                ),
              );
            },
          ),
        ),
        backgroundColor: Colors.pink[100],
        body: SafeArea(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedOpacity(
                  opacity: _visible ? 1.0 : 0.0,
                  duration: const Duration(milliseconds: 5000),
                  child: Visibility(
                    visible: _visible,
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: size.height * 0.05),
                          width: size.width,
                          height: size.height * 0.4,
                          decoration: BoxDecoration(
                            color: Colors.pink[200],
                            border: Border.all(width: 3),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(imagePath.toString()),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: size.width * 0.6,
                          height: size.height * 0.1,
                          child: FloatingActionButton.extended(
                            heroTag: "btn1",
                            backgroundColor: const Color(0xff03dac6),
                            foregroundColor: Colors.black,
                            onPressed: () {
                              setState(() {
                                _visible = !_visible;
                              });
                            },
                            tooltip: 'JUGAR',
                            label: Text(
                              "JUGAR",
                              style: TextStyle(fontSize: size.width * 0.08),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                AnimatedOpacity(
                  opacity: _visible ? 0.0 : 1.0,
                  duration: const Duration(milliseconds: 5000),
                  child: Visibility(
                    visible: !_visible,
                    child: Column(
                      children: [
                        Text(
                          question,
                          style: TextStyle(
                            fontSize: size.width * 0.07,
                            color: Colors.purple,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 20),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: answers.length,
                          itemBuilder: (context, index) {
                            final respuesta = answers[index];
                            final respuestaText = respuesta['respuesta'];
                            final esCorrecta = respuesta['esCorrecta'];
                            return Container(
                              margin: EdgeInsets.only(
                                  bottom: size.height * 0.02,
                                  left: size.width * 0.1,
                                  right: size.width * 0.1),
                              child: ListTile(
                                shape: RoundedRectangleBorder(
                                  side: const BorderSide(
                                      width: 2, color: Colors.purple),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                title: Text(
                                  respuestaText,
                                  style: TextStyle(fontSize: size.width * 0.07),
                                  textAlign: TextAlign.center,
                                ),
                                onTap: () {
                                  setState(() {
                                    if (esCorrecta) {
                                      points += 100;
                                      alertRespuestas(
                                        context,
                                        "Respuesta Correcta",
                                        "¡Muy bien!",
                                        Colors.lightGreenAccent,

                                      );
                                      changeQuestion();
                                    } else {
                                      alertRespuestas(
                                        context,
                                        "Respuesta Incorrecta",
                                        "¡Suerte la próxima!",
                                        Colors.white,

                                      );
                                      changeQuestion();
                                    }
                                  });
                                },
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
