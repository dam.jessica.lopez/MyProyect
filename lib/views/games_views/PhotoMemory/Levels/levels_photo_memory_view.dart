import 'package:flutter/material.dart';
import 'package:my_proyect/views/components_views/bar_menu.dart';
import '../../../components_views/level_button.dart';
import 'Level2/photo_memory2.dart';
import 'Level1/photo_memory1.dart';
import 'Level3/photo_memory3.dart';

class NivelesPhotoMemoryView extends StatefulWidget{
  const NivelesPhotoMemoryView({super.key});

  @override
  State<StatefulWidget> createState() {
    return _NivelesPhotoMemoryView();
  }
}


class _NivelesPhotoMemoryView extends State<NivelesPhotoMemoryView>{
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('RECUERDA',textAlign: TextAlign.center,style: TextStyle(fontSize: 30)),
          backgroundColor: Colors.purple,
          elevation: 30,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const BarMenu(),
                ),
              );
            },
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.info),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Información'),
                      content: const Text('Este juego esta basado en ejercitar la memoria.\n\nConsta de una pantalla donde se muestra una fotografía, tras presionar el botón jugar esta procederá a desaparecer y se mostrarán preguntas respecto a ella.\n\nPara avanzar a la siguiente operacion deberá presionar uno de los tres botones. Se podrá avanzar o cambiar de pregunta mediante el botón "Siguiente" en la parte inferior. \nTras realizar 5 preguntas se mostrará una puntuación y se terminará la partida.'),
                      actions: [
                        TextButton(
                          child: const  Text('Cerrar'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          ],
        ),
        backgroundColor: Colors.lightBlue,
        body: SafeArea(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const SizedBox(
                height: 60,
              ),
    
              LevelButton(text: "NIVEL 1", customColor: Colors.purple.shade300, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PhotoMemory1(),
                  ),
                );
              }),
    
              LevelButton(text: "NIVEL 2", customColor: Colors.purple, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PhotoMemory2(),
                  ),
                );
              }),
    
              LevelButton(text: "NIVEL 3", customColor: Colors.purple.shade800, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PhotoMemory3(),
                  ),
                );
              }),
            ],
          ),
        ),
    
      ),
    );
  }
}