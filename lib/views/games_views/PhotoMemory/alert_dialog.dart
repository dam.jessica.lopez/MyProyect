import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/PhotoMemory/Levels/levels_photo_memory_view.dart';

Future<void> showEndGameMemory(
    BuildContext context, String title, String content, VoidCallback saveMatch,
    {bool barrierDismissible = false}) {
  return showDialog<void>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: 27)),
          content: Text(content, textAlign: TextAlign.center, style: TextStyle(fontSize: 23)),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              onPressed: () {
                saveMatch();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const NivelesPhotoMemoryView(),
                    ));
              },
              child: const Text('SI', style: TextStyle(fontSize: 25)),
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              onPressed: () {
                saveMatch();
                Navigator.popAndPushNamed(context, '/bar');
              },
              child: const Text('NO', style: TextStyle(fontSize: 25)),
            ),
          ],
        );
      });
}

Future<void> alertRespuestas(
    BuildContext context, String title, String content, Color color) {
  return showDialog<void>(
      //barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            backgroundColor: color,
            title: Text(
              title,
              style: const TextStyle(fontSize: 25),
              textAlign: TextAlign.center,
            ),
            content: Text(
              content,
              style: const TextStyle(fontSize: 25),
              textAlign: TextAlign.center,
            ),
            );
      });
}
