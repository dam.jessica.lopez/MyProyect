import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../../../controller/singleton/DataHolder.dart';
import '../../../../../model/PUGame.dart';
import '../../../../../model/Partida.dart';
import '../../../../../model/PerfilUser.dart';
import '../../../../components_views/bar_menu.dart';
import '../levels_quiz_view.dart';
import 'question3.dart';


class QuestionsScreen3 extends StatefulWidget {
  const QuestionsScreen3({Key? key}) : super(key: key);

  @override
  State<QuestionsScreen3> createState() => _QuestionsScreen3State();
}

class _QuestionsScreen3State extends State<QuestionsScreen3> {

  final List<Question3> questions = QuestionData3().questions;

  int index = 0;
  int score = 0;
  bool falseUserAnswer = false;
  bool trueUserAnswer = true;

 final level = 3;
  final String key = "QUIZ";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;

  @override
  void initState() {
    super.initState();
    getKeyGame();
  }
   void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      //print('Clave:' + key + ' ' + pUG.uid);
    } else {
      //print('No existe la clave');
    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = score;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }

  Future<void> showAnswer(bool correctAnswer) async {
    String titleDialog = (correctAnswer) ? 'Era cierto ' : 'Era falso. ';
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text(titleDialog, textAlign: TextAlign.center, style: TextStyle(fontSize: 27)),
          children: [
            Text(questions[index].explication, textAlign: TextAlign.center, style: TextStyle(fontSize: 23)),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                nextQuestion();
              },
              child: const Text('Siguiente pregunta.', style: TextStyle(fontSize: 25)),
            ),
          ],
        );
      },
    );
  }

  void nextQuestion() {
    if(index < questions.length - 1) {
      index++;
      setState(() {});
    } else {
      showresult();

    }
  }

  Future<void> showresult() async {
    return await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: const Text('TERMINAMOS!', textAlign: TextAlign.center, style: TextStyle(fontSize: 27)),
            content: Text('Tu puntuación es: $score puntos. \n ¿Quieres volver a los niveles?', textAlign: TextAlign.center,
            style: TextStyle(fontSize: 23)),
            actions: [
              TextButton(
                onPressed: () {
                  saveMatch();
                  Navigator.pop(ctx);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>  const NivelesQuizView(),
                    ),
                  );
                },
                child: const Text('SI', style: TextStyle(fontSize: 25)),
              ),
              TextButton(
                onPressed: () {
                  saveMatch();
                  Navigator.pop(ctx);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>  const BarMenu(),
                    ),
                  );
                },
                child: const Text('NO', style: TextStyle(fontSize: 25)),
              ),
            ],
          );
        });
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final Question3 question = questions[index];
    return Scaffold(
      appBar: AppBar(
        title: Text('ELIGE LA CORRECTA',
            style: TextStyle(fontSize: 30)),
        backgroundColor: Colors.purple,
        elevation: 10,
      ),
      backgroundColor: Colors.purple[100],
      body:SafeArea(
      child:Container(
        height: MediaQuery.of(context).size.height * 1,
        margin: const EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text('Pregunta número: ${index + 1} / ${questions.length}', style: TextStyle(fontSize: size.width * 0.07, fontStyle: FontStyle.italic)),
            Text(question.title, 
             textAlign: TextAlign.center,
            style: TextStyle(fontSize: size.width * 0.08, fontWeight: FontWeight.w500),),
            SizedBox(
              height: 350,
              width: 350,
              child:
                question.imageUrl,


            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.purple,
                      minimumSize: Size(size.width * 0.3, size.width * 0.15)
                    ),
                    onPressed: () {
                      showAnswer(question.answer);
                      setState(() {
                        if (falseUserAnswer == questions[index].answer) {
                          score += 100;
                        } else {
                          score += 0;
                        }
                      });
                    },
                    child: const Text('FALSO'),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.purple,
                      minimumSize: Size(size.width * 0.3, size.width * 0.15)
                    ),
                    onPressed: () {
                      showAnswer(question.answer);
                      if (trueUserAnswer == questions[index].answer) {
                        score += 100;
                      } else {
                        score += 0;
                      }
                    },
                    child: const Text('VERDADERO'),
                  ),
                ],
              ),
          ],
        ),
      ),
    ),
    );
  }
}