import 'package:flutter/cupertino.dart';

class Question3 {
  final String title;
  final Image imageUrl;
  final bool answer;
  final String explication;

  Question3({
    required this.title,
    required this.imageUrl,
    required this.answer,
    required this.explication,
  });
}

class QuestionData3 {
  List<Question3> questions = [
    Question3(
      title: '¿Correcto?-"El verano empieza en el mes de Junio"',
      imageUrl: Image.asset('assets/images/questions/questions3/verano.jpg'),
      answer: true,
      explication: 'Es correcto',
    ),
    Question3(
      title: '¿La Tierra es plana?',
      imageUrl: Image.asset('assets/images/questions/questions3/tierra.jpg'),
      answer: false,
      explication: 'No, la Tierra es un planeta esférico.',
    ),
    Question3(
      title: '¿Es una cámara?',
      imageUrl: Image.asset('assets/images/questions/questions3/camara.jpg'),
      answer: true,
      explication: 'Si, es una cámara, las hay de varios tipos.',
    ),
    Question3(
      title: '¿La ballena es un pez?',
      imageUrl: Image.asset('assets/images/questions/questions3/ballena.jpg'),
      answer: false,
      explication: 'Falso, es un mamifero.',
    ),
    Question3(
      title: '¿La torre de pisa se encuentra en Paris?',
      imageUrl: Image.asset('assets/images/questions/questions3/pisa.jpg'),
      answer: false,
      explication: 'Falso, se encuentra en Italia',
    ),
    Question3(
      title: '¿Los koalas son animales nativos de Australia?',
      imageUrl: Image.asset('assets/images/questions/questions3/koala.jpg'),
      answer: true,
      explication: 'En efecto, son de Australia.',
    ),
    Question3(
      title: '¿Los elefantes son animales acuáticos?',
      imageUrl: Image.asset('assets/images/questions/questions3/elefante.jpg'),
      answer: false,
      explication: 'No, los elefantes son animales terrestres.',
    ),
    Question3(
      title: '¿El sol es una estrella?.',
      imageUrl: Image.asset('assets/images/questions/questions3/sol.jpg'),
      answer: true,
      explication: 'Sí, el sol es una estrella.',
    ),
    Question3(
      title: '¿El inglés es el idioma más hablado del mundo?',
      imageUrl: Image.asset('assets/images/questions/questions3/ingles.jpg'),
      answer: false,
      explication: 'No, el chino mandarín es el idioma más hablado del mundo.',
    ),
     Question3(
      title: 'El acueducto de Segovia es una construcción romana.',
      imageUrl: Image.asset('assets/images/questions/questions3/acueducto_segovia.jpg'),
      answer: true,
      explication: 'Sí, el acueducto de Segovia es una construcción romana.',
    ),
  ];
}
