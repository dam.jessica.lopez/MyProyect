import 'package:flutter/cupertino.dart';

class Question {
  final String title;
  final Image imageUrl;
  final bool answer;
  final String explication;

  Question({
    required this.title,
    required this.imageUrl,
    required this.answer,
    required this.explication,
  });
}

class QuestionData {
  List<Question> questions = [
    Question(
      title: '¿Correcto?-"Eso lo va a hacer Rita la Cantaora"',
      imageUrl:
          Image.asset('assets/images/questions/questions1/expresiones.jpg'),
      answer: true,
      explication: 'Un antiguo dicho.',
    ),
    Question(
      title: '¿Es una manzana?',
      imageUrl: Image.asset('assets/images/questions/questions1/manzana.png'),
      answer: true,
      explication: 'Si, es una manzana\nLas hay de varios tipos.',
    ),
    Question(
      title: '¿Un año tiene 345 días?',
      imageUrl:
          Image.asset('assets/images/questions/questions1/calendario.jpg'),
      answer: false,
      explication: 'Falso,tiene 365 días.\nEn años bisiestos 366.',
    ),
    Question(
      title: 'El arcoiris tiene 3 colores',
      imageUrl: Image.asset('assets/images/questions/questions1/arcoiris.png'),
      answer: false,
      explication: 'El arcoiris esta formado por 7 colores',
    ),
    Question(
      title: 'El mejor amigo del hombre es el perro.',
      imageUrl: Image.asset('assets/images/questions/questions1/perro.jpg'),
      answer: true,
      explication: 'En efecto, es el perro.',
    ),
    Question(
      title: 'Hay tres estaciones del año.',
      imageUrl:
          Image.asset('assets/images/questions/questions1/estaciones.jpg'),
      answer: false,
      explication:
          'Tenemos cuatro estaciones: \nPrimavera,\nVerano,\nOtoño,\nInvierno.',
    ),
    Question(
      title: '¿Es la letra "d" la cuarta en el abecedario?.',
      imageUrl:
          Image.asset('assets/images/questions/questions1/abecedario.jpg'),
      answer: true,
      explication: 'Si, lo es. Las anteriores son "a, b ,c".',
    ),
    Question(
      title: '¿El agua hierve a 100 grados Celsius?',
      imageUrl: Image.asset('assets/images/questions/questions1/agua.jpg'),
      answer: true,
      explication:
          'Sí, bajo condiciones normales, el agua hierve a 100 grados Celsius.',
    ),
    Question(
      title: '¿El oxígeno es necesario para la combustión?',
      imageUrl:
          Image.asset('assets/images/questions/questions1/combustion.jpg'),
      answer: true,
      explication: 'Sí, el oxígeno es esencial para que ocurra la combustión.',
    ),
    Question(
      title: '¿El agua es un compuesto químico?',
      imageUrl:
          Image.asset('assets/images/questions/questions1/agua_quimica.jpg'),
      answer: true,
      explication:
          'Sí, el agua es un compuesto químico formado por dos átomos de hidrógeno y uno de oxígeno.',
    ),
  ];
}
