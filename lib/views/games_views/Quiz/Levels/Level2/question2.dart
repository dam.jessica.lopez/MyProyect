import 'package:flutter/cupertino.dart';

class Question2 {
  final String title;
  final Image imageUrl;
  final bool answer;
  final String explication;

  Question2({
    required this.title,
    required this.imageUrl,
    required this.answer,
    required this.explication,
  });
}

class QuestionData2 {
  List<Question2> questions = [
    Question2(
      title: '¿Correcto?-"¿Tenemos 5 sentidos?"',
      imageUrl: Image.asset('assets/images/questions/questions2/sentidos.jpg'),
      answer: true,
      explication: 'Sí, es la respuesta correcta',
    ),
    Question2(
      title: '¿La II Guerra Mundial acabó en 1945?',
      imageUrl: Image.asset('assets/images/questions/questions2/war.jpg'),
      answer: true,
      explication: 'Si, acabó en ese año.',
    ),
    Question2(
      title: 'Los colores de la bandera de España son rojo y azul',
      imageUrl: Image.asset('assets/images/questions/questions2/map.jpg'),
      answer: false,
      explication: 'Falso, los colores de la bandera son rojo y amarillo.',
    ),
    Question2(
      title: 'Hay 20 horas en un día',
      imageUrl: Image.asset('assets/images/questions/questions2/clock.jpg'),
      answer: false,
      explication: 'El arcoiris esta formado por 7 colores',
    ),
    Question2(
      title: '¿Vivimos en el siglo 21?',
      imageUrl: Image.asset('assets/images/questions/questions2/years.jpg'),
      answer: true,
      explication: 'En efecto, es el siglo 21.',
    ),
    Question2(
      title: 'Hay 10 meses en un año',
      imageUrl: Image.asset('assets/images/questions/questions2/meses.jpg'),
      answer: false,
      explication: 'No es cierto, hay 12 meses.',
    ),
    Question2(
      title: 'La hija de tu hija es tu nieta',
      imageUrl: Image.asset('assets/images/questions/questions2/nieto.jpg'),
      answer: true,
      explication: 'Si, lo es.',
    ),
    Question2(
      title: '¿La Mona Lisa fue pintada por Leonardo da Vinci?',
      imageUrl: Image.asset('assets/images/questions/questions2/mona_lisa.jpg'),
      answer: true,
      explication:
          'Sí, la Mona Lisa es una famosa pintura realizada por Leonardo da Vinci.',
    ),
    Question2(
      title: '¿El cuerpo humano tiene 206 huesos?',
      imageUrl: Image.asset('assets/images/questions/questions2/huesos.jpg'),
      answer: true,
      explication: 'Sí, el cuerpo humano tiene aproximadamente 206 huesos.',
    ),
    Question2(
      title: 'El Monte Fuji es el volcán más alto de Japón.',
      imageUrl: Image.asset('assets/images/questions/questions2/mount_fuji.jpg'),
      answer: true,
      explication: 'Sí, el Monte Fuji es el volcán más alto de Japón.',
    ),
  ];
}
