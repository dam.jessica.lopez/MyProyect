

import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/Frase/levels_phrases_view.dart';

import '../../../../controller/singleton/DataHolder.dart';
import '../../../../model/PUGame.dart';
import '../../../../model/Partida.dart';
import '../../../../model/PerfilUser.dart';
import '../../../components_views/bar_menu.dart';



class Frase3 extends StatefulWidget {
  const Frase3({super.key});

  @override
  _Frase3State createState() => _Frase3State();
}

class _Frase3State extends State<Frase3> {



  List<Map<String, dynamic>> pJson = [];
  List<bool> corrects = [];

  late int puntos=0;
  late int round=0;

  bool falseAnswer = false;
  bool trueAnswer = true;

  List<int> questionIndices = [];


  final level = 3;
  final String key = "FRASE";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;




  @override
  void initState() {
    super.initState();
    generateRandomQuestions();
    getKeyGame();
  }
  void verificarRespuesta(int index, int seleccionada) {
    final preguntaRespuesta = pJson[index];
    final correcta = preguntaRespuesta['correcta'];

    setState(() {
      corrects[index] = seleccionada == correcta;
    });
  }

  void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      //print('Clave:' + key + ' ' + pUG.uid);

    } else {
      //print('No existe la clave');
    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = puntos;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }



  void generateRandomQuestions() {
    final random = Random();
    pJson = [
      {
        "pregunta": "Dar un consejo sobre algo a una persona.",
        "respuestas": [
          {"respuesta": "PRESIONAR", "esCorrecta": false ,"habilitada": true },
          {"respuesta": "RECOMENDAR", "esCorrecta": true,"habilitada": true },
          {"respuesta": "ADMIRAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "OLVIDAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "VER", "esCorrecta": false,"habilitada": true },
        ],
      },
      {
        "pregunta": "Prenda hecha de toalla con forma de bata, que nos seca al salir de la ducha.",
        "respuestas": [
          {"respuesta": "ALFÉREZ", "esCorrecta": false ,"habilitada": true },
          {"respuesta": "ALBÚMINA", "esCorrecta": false,"habilitada": true },
          {"respuesta": "BATÍN", "esCorrecta": false,"habilitada": true },
          {"respuesta": "ALBATROS", "esCorrecta": false,"habilitada": true },
          {"respuesta": "ALBORNOZ", "esCorrecta": true,"habilitada": true },
        ],
      },
      {
        "pregunta": "Hacer el diseño de una publicación para imprimirla.",
        "respuestas": [
          {"respuesta": "MAQUETAR", "esCorrecta": true ,"habilitada": true },
          {"respuesta": "MAQUINAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "MARCAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "MOVER", "esCorrecta": false,"habilitada": true },
          {"respuesta": "MIRAR", "esCorrecta": false,"habilitada": true },
        ],
      },
      {
        "pregunta": "Objeto para colgar y llevar las llaves.",
        "respuestas":  [
          {"respuesta": "CAMISA", "esCorrecta": false,"habilitada": true },
          {"respuesta": "RELOJ", "esCorrecta": false,"habilitada": true },
          {"respuesta": "LLAVERO", "esCorrecta": true,"habilitada": true },
          {"respuesta": "COLLAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "PULSERA", "esCorrecta": false,"habilitada": true },
        ],
      },
      {
        "pregunta": "Entrometerse en un asunto sin tener permiso ni autoridad.",
        "respuestas": [
          {"respuesta": "IRRITAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "PECAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "ROBAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "INMISCUIR", "esCorrecta": true,"habilitada": true },
          {"respuesta": "ROER", "esCorrecta": false,"habilitada": true },
        ],

      },
      {
        "pregunta": "Maquillaje para resaltar las pestañas.",
        "respuestas": [
          {"respuesta": "RIMEL", "esCorrecta": true,"habilitada": true },
          {"respuesta": "TOPAZ", "esCorrecta": false,"habilitada": true },
          {"respuesta": "ESMALTE", "esCorrecta": false,"habilitada": true },
          {"respuesta": "LIMA", "esCorrecta": false,"habilitada": true },
          {"respuesta": "CERA", "esCorrecta": false,"habilitada": true },
        ],
      },
      {
        "pregunta": "Se dice cuando el Estado se apropia de los bienes de una persona.",
        "respuestas": [
          {"respuesta": "LLEVAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "PULSAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "ESCACIAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "CONFISCAR", "esCorrecta": true,"habilitada": true },
          {"respuesta": "CONVIDAR", "esCorrecta": false,"habilitada": true },
        ],
      },

      {
        "pregunta": "Comida preparada al fuego con salsa y varios ingredientes.",
        "respuestas": [
          {"respuesta": "TARTA", "esCorrecta": false,"habilitada": true },
          {"respuesta": "GUISO", "esCorrecta": true,"habilitada": true },
          {"respuesta": "GALLETA", "esCorrecta": false,"habilitada": true },
          {"respuesta": "PASTEL", "esCorrecta": false,"habilitada": true },
          {"respuesta": "AJO", "esCorrecta": false,"habilitada": true },
        ],
      },
      {
        "pregunta": "Formarse una idea a partir de pocos datos y sin tener seguridad sobre ello.",
        "respuestas":  [
          {"respuesta": "SABER", "esCorrecta": false,"habilitada": true },
          {"respuesta": "SUPONER", "esCorrecta": true,"habilitada": true },
          {"respuesta": "ADMIRAR", "esCorrecta": false,"habilitada": true },
          {"respuesta": "APRENDER", "esCorrecta": false,"habilitada": true },
          {"respuesta": "LEER", "esCorrecta": false,"habilitada": true },
        ],
      },
      {
        "pregunta": "Rueda metálica con dientes que se ajusta a una cadena.",
        "respuestas":  [
          {"respuesta": "PIÑA", "esCorrecta": false,"habilitada": true },
          {"respuesta": "MOSQUETÓN", "esCorrecta": false,"habilitada": true },
          {"respuesta": "PIÑÓN", "esCorrecta": true,"habilitada": true },
          {"respuesta": "RIÑÓN", "esCorrecta": false,"habilitada": true },
          {"respuesta": "RATÓN", "esCorrecta": false,"habilitada": true },
        ],
      },
    ];



    pJson.shuffle(random);
    for (var preguntaRespuesta in pJson) {
      corrects.add(false);
    }
  }


  void checkAnswer(int index, int seleccionada) {
    final preguntaRespuesta = pJson[index];
    final respuestas = preguntaRespuesta['respuestas'];

    setState(() {
      if (respuestas[seleccionada]['esCorrecta']) {
        puntos += 100;
      }
    });

    nextQuestion();
  }


  Future<void> showAnswer(bool correctAnswer) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: const Text(""),
          children: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                nextQuestion();
              },
              child: const Text('Siguiente pregunta.'),
            ),
          ],
        );
      },
    );
  }

  void nextQuestion() {
    if (round < pJson.length - 1) {
      round++;
    } else {
      showResult();
    }
  }

  Future<void> showResult() async {
    return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) {
        return AlertDialog(
          title: const Text('¡HAS TERMINADO!', textAlign: TextAlign.center, style: TextStyle(fontSize: 27)),
          content: Text(
            'Puntuación total: $puntos \n ¿Quieres volver a los niveles?',
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 23),
          ),
          actions: [
            TextButton(
              onPressed: () {
                saveMatch();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const NivelesFrasesView(),
                  ),
                );
              },
              child: const Text('SI', style: TextStyle(fontSize: 25)),
            ),
            TextButton(
              onPressed: () {
                saveMatch();
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const BarMenu(),
                  ),
                );
              },
              child: const Text('NO', style: TextStyle(fontSize: 25)),
            ),
          ],
        );
      },
    );
}


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text('COMPLETA LA FRASE',style: TextStyle(fontSize: 30)),
        backgroundColor: Colors.deepPurple,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>  NivelesFrasesView(),
              ),
            );
          },
        ),
      ),
      backgroundColor: Colors.deepPurple[100],
      body:   SafeArea(
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.7,
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

              Text('Ronda: ${round + 1} / ${pJson.length}', style: const TextStyle(fontSize: 25)),
              Text(  pJson[round]['pregunta'], textAlign: TextAlign.center, style: const TextStyle(  fontSize: 25, fontWeight: FontWeight.w500),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 0);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][0]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 1);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][1]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 2);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][2]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 3);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][3]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 4);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][4]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
              ]
            )
          ),
        ),
      ),
    );
  }
}
