

import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/Frase/levels_phrases_view.dart';

import '../../../../controller/singleton/DataHolder.dart';
import '../../../../model/PUGame.dart';
import '../../../../model/Partida.dart';
import '../../../../model/PerfilUser.dart';
import '../../../components_views/bar_menu.dart';



class Frase1 extends StatefulWidget {
  const Frase1({super.key});

  @override
  _Frase1State createState() => _Frase1State();
}

class _Frase1State extends State<Frase1> {


  List<Map<String, dynamic>> pJson = [];
  List<bool> corrects = [];

  late int puntos=0;
  late int round=0;

  bool falseAnswer = false;
  bool trueAnswer = true;

  List<int> questionIndices = [];


  final level = 1;
  final String key = "FRASE";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;


  @override
  void initState() {
    super.initState();
    generateRandomQuestions();
    getKeyGame();
  }
  void verificarRespuesta(int index, int seleccionada) {
    final preguntaRespuesta = pJson[index];
    final correcta = preguntaRespuesta['correcta'];

    setState(() {
      corrects[index] = seleccionada == correcta;
    });
  }

  void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      //print('Clave:' + key + ' ' + pUG.uid);

    } else {
      //print('No existe la clave');
    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = puntos;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
    
  }


  void generateRandomQuestions() {
    final random = Random();
    pJson = [
      {
        "pregunta": "Lugar al que van los niños para aprender:",
        "respuestas": [
          {"respuesta": "CORDÓN", "esCorrecta": false},
          {"respuesta": "COLEGIO", "esCorrecta": true},
          {"respuesta": "CASTOR", "esCorrecta": false},
        ],
      },
      {
        "pregunta": "Objeto para guardar la ropa y llevar cosas en un viaje, con un asa.",
        "respuestas": [
          {"respuesta": "MACETA", "esCorrecta": false},
          {"respuesta": "SACO", "esCorrecta": false},
          {"respuesta": "MALETA", "esCorrecta": true},
        ],
      },
      {
        "pregunta": "Objeto que sirve para echar sal",
        "respuestas":  [
          {"respuesta": "SALERO", "esCorrecta": true},
          {"respuesta": "ESPALDA", "esCorrecta": false},
          {"respuesta": "NIÑA", "esCorrecta": false},
        ],
      },
      {
        "pregunta": "Trozo cuadrado de tela que se usa para secarse",
        "respuestas":  [
          {"respuesta": "TOALLA", "esCorrecta": true},
          {"respuesta": "DÁTIL", "esCorrecta": false},
          {"respuesta": "CARMÍN", "esCorrecta": false},
        ],
      },
      {
        "pregunta": "Borde de carne alrededor de la boca, con el que besamos.",
        "respuestas":  [
          {"respuesta": "LABIO", "esCorrecta": true},
          {"respuesta": "DIENTE", "esCorrecta": false},
          {"respuesta": "OÍDO", "esCorrecta": false},
        ],
      },
      {
        "pregunta": "Lugar en el que se practica la natación.",
        "respuestas":  [
          {"respuesta": "PLAZA", "esCorrecta": false},
          {"respuesta": "PISCINA", "esCorrecta": true},
          {"respuesta": "PORTERÍA", "esCorrecta": false},
        ],
      },
      {
        "pregunta": "Palo de metal o de madera para apoyarse al caminar.",
        "respuestas": [
          {"respuesta": "PISTÓN", "esCorrecta": false},
          {"respuesta": "RUEDA", "esCorrecta": false},
          {"respuesta": "BASTÓN", "esCorrecta": true},
        ],
      },
      {
        "pregunta": "Parte anterior de la cabeza que va desde los ojos hasta el pelo.",
        "respuestas": [
          {"respuesta": "NARIZ", "esCorrecta": false},
          {"respuesta": "FUENTE", "esCorrecta": false},
          {"respuesta": "FRENTE", "esCorrecta": true},
        ],
      },
      {
        "pregunta": "Prenda de vestir larga que se ata a la cintura y se usa en casa.",
        "respuestas":  [
          {"respuesta": "ZAPATO", "esCorrecta": false},
          {"respuesta": "BATA", "esCorrecta": true},
          {"respuesta": "TAPA", "esCorrecta": false},
        ],
      },
      {
        "pregunta": "Pelos que están por encima de los ojos.",
        "respuestas": [
          {"respuesta": "CENA", "esCorrecta": false},
          {"respuesta": "CEJA", "esCorrecta": true},
          {"respuesta": "PIEL", "esCorrecta": false},
        ],
      }
    ];



    pJson.shuffle(random);
    for (var preguntaRespuesta in pJson) {
      corrects.add(false);
    }
  }

  void checkAnswer(int index, int seleccionada) {
    final preguntaRespuesta = pJson[index];
    final respuestas = preguntaRespuesta['respuestas'];

    setState(() {
      if (respuestas[seleccionada]['esCorrecta']) {
        puntos += 100;
      }
    });

    nextQuestion();
  }


  Future<void> showAnswer(bool correctAnswer) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: const Text(""),
          children: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                nextQuestion();
              },
              child: const Text('Siguiente pregunta.'),
            ),
          ],
        );
      },
    );
  }

  void nextQuestion() {
    if (round < pJson.length - 1) {
      round++;
    } else {
      showResult();
    }
  }

  Future<void> showResult() async {
    return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) {
        return AlertDialog(
          title: const Text('¡HAS TERMINADO!', style: TextStyle(fontSize: 27)),
          content: Text(
            'Puntuación total: $puntos \n ¿Quieres volver a los niveles?',
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 23),
          ),
          actions: [
            TextButton(
              onPressed: () {
                saveMatch();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const NivelesFrasesView(),
                  ),
                );
              },
              child: const Text('SI', style: TextStyle(fontSize: 25)),
            ),
            TextButton(
              onPressed: () {
                saveMatch();
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const BarMenu(),
                  ),
                );
              },
              child: const Text('NO', style: TextStyle(fontSize: 25)),
            ),
          ],
        );
      },
    );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('COMPLETA LA FRASE',style: TextStyle(fontSize: 30)),
        backgroundColor: Colors.deepPurple,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>  NivelesFrasesView(),
              ),
            );
          },
        ),
      ),
      backgroundColor: Colors.deepPurple[100],
      body:   SafeArea(
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.7,
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

              Text('Ronda: ${round + 1} / ${pJson.length}', style: const TextStyle(fontSize: 25)),
              Text(  pJson[round]['pregunta'], textAlign: TextAlign.center, style: const TextStyle(  fontSize: 25, fontWeight: FontWeight.w500),),

                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 0);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][0]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 1);
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.deepPurple,
                    minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][1]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 2);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][2]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),

              ]
            )
          ),
        ),
      ),
    );
  }
}
