

import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/Frase/levels_phrases_view.dart';

import '../../../../controller/singleton/DataHolder.dart';
import '../../../../model/PUGame.dart';
import '../../../../model/Partida.dart';
import '../../../../model/PerfilUser.dart';
import '../../../components_views/bar_menu.dart';



class Frase2 extends StatefulWidget {
  const Frase2({super.key});

  @override
  _Frase2State createState() => _Frase2State();
}

class _Frase2State extends State<Frase2> {



  List<Map<String, dynamic>> pJson = [];
  List<bool> corrects = [];

  late int puntos=0;
  late int round=0;

  bool falseAnswer = false;
  bool trueAnswer = true;

  List<int> questionIndices = [];

  final level = 2;
  final String key = "FRASE";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;


  @override
  void initState() {
    super.initState();
    generateRandomQuestions();
    getKeyGame();

  }
  void verificarRespuesta(int index, int seleccionada) {
    final preguntaRespuesta = pJson[index];
    final correcta = preguntaRespuesta['correcta'];

    setState(() {
      corrects[index] = seleccionada == correcta;
    });
  }

  void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      //print('Clave:' + key + ' ' + pUG.uid);

    } else {
      //print('No existe la clave');
    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = puntos;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }

  void generateRandomQuestions() {
    final random = Random();
    pJson = [
      {
        "pregunta": "Heramienta de metal que se usa para apalancar y abrir.",
        "respuestas": [
          {"respuesta": "PINZA", "esCorrecta": false,"habilitada": true },
          {"respuesta": "PALANCA", "esCorrecta": true,"habilitada": true},
          {"respuesta": "PALO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "PLOMO", "esCorrecta": false,"habilitada": true},
        ],
      },
      {
        "pregunta": "Parte inferior y alargada de la mandibula.",
        "respuestas": [
          {"respuesta": "LENGUA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "PECHO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "NUCA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "MENTÓN", "esCorrecta": true,"habilitada": true},
        ],
      },
      {
        "pregunta": "Medio de transporte con trenes que van bajo tierra.",
        "respuestas": [
          {"respuesta": "SUBMARINO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "PUERTO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "METRO", "esCorrecta": true,"habilitada": true},
          {"respuesta": "CARRO", "esCorrecta": false,"habilitada": true},
        ],
      },
      {
        "pregunta": "Cicatriz redonda y pequeña en el vientre, resultado de cortar el cordón umbilical.",
        "respuestas": [
          {"respuesta": "OMBLIGO", "esCorrecta": true,"habilitada": true},
          {"respuesta": "HERIDA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "TRIPA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "UÑA", "esCorrecta": false,"habilitada": true},
        ],
      },
      {
        "pregunta": "Hijo de mi tío.",
        "respuestas": [
          {"respuesta": "NIETO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "YERNO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "PRIMO", "esCorrecta": true,"habilitada": true},
          {"respuesta": "HERMANO", "esCorrecta": false,"habilitada": true},
        ],
      },

      {
        "pregunta": "Persona que vende y representa productos.",
        "respuestas": [
          {"respuesta": "CAMARERA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "DIRECTOR", "esCorrecta": false,"habilitada": true},
          {"respuesta": "COMERCIAL", "esCorrecta": true,"habilitada": true},
          {"respuesta": "CÓMICO", "esCorrecta": false,"habilitada": true},
        ],
      },
      {
        "pregunta": "Pescado comestible, con carne rosada y anaranjada, que remonta los ríos para poner sus huevos.",
        "respuestas": [
          {"respuesta": "SAPO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "SALMÓN", "esCorrecta": true,"habilitada": true},
          {"respuesta": "SARDINA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "SILURO", "esCorrecta": false,"habilitada": true},
        ],
      },
      {
        "pregunta": "Piel que cubre y protege el ojo, puede subir y bajar.",
        "respuestas": [
          {"respuesta": "PARAGUAS", "esCorrecta": false,"habilitada": true},
          {"respuesta": "PÁRPADO", "esCorrecta": true,"habilitada": true},
          {"respuesta": "CORTINA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "CEJA", "esCorrecta": false,"habilitada": true},
        ],
      },
      {
        "pregunta": "Piel de los animales tratada para hacer ropa o bolsos.",
        "respuestas": [
          {"respuesta": "CORCHO", "esCorrecta": false,"habilitada": true},
          {"respuesta": "LANA", "esCorrecta": false,"habilitada": true},
          {"respuesta": "CUERO", "esCorrecta": true,"habilitada": true},
          {"respuesta": "ALGODÓN", "esCorrecta": false,"habilitada": true},
        ],
      },
      {
        "pregunta": "Sentimiento de terror hacia algo peligroso.",
        "respuestas": [
          {"respuesta": "MIEDO", "esCorrecta": true,"habilitada": true},
          {"respuesta": "AMOR", "esCorrecta": false,"habilitada": true},
          {"respuesta": "DOLOR", "esCorrecta": false,"habilitada": true},
          {"respuesta": "ASCO", "esCorrecta": false,"habilitada": true},
        ],
      },
    ];



    pJson.shuffle(random);
    for (var preguntaRespuesta in pJson) {
      corrects.add(false);
    }
  }
  void checkAnswer(int index, int seleccionada) {
    final preguntaRespuesta = pJson[index];
    final respuestas = preguntaRespuesta['respuestas'];

    setState(() {
      if (respuestas[seleccionada]['esCorrecta']) {
        puntos += 100;
      }
    });

    nextQuestion();
  }


  Future<void> showAnswer(bool correctAnswer) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: const Text(""),
          children: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                nextQuestion();
              },
              child: const Text('Siguiente pregunta.'),
            ),
          ],
        );
      },
    );
  }

  void nextQuestion() {
    if (round < pJson.length - 1) {
      round++;
    } else {
      showResult();
    }
  }

  Future<void> showResult() async {
    return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) {
        return AlertDialog(
          title: const Text('¡HAS TERMINADO!', textAlign: TextAlign.center, style: TextStyle(fontSize: 27)),
          content: Text(
            'Puntuación total: $puntos \n ¿Quieres volver a los niveles?',
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 23),
          ),
          actions: [
            TextButton(
              onPressed: () {
                saveMatch();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const NivelesFrasesView(),
                  ),
                );
              },
              child: const Text('SI', style: TextStyle(fontSize: 25)),
            ),
            TextButton(
              onPressed: () {
                saveMatch();
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const BarMenu(),
                  ),
                );
              },
              child: const Text('NO', style: TextStyle(fontSize: 25)),
            ),
          ],
        );
      },
    );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('COMPLETA LA FRASE',style: TextStyle(fontSize: 30)),
        backgroundColor: Colors.deepPurple,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>  NivelesFrasesView(),
              ),
            );
          },
        ),
      ),
      backgroundColor: Colors.deepPurple[100],
      body:   SafeArea(
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.7,
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

              Text('Ronda: ${round + 1} / ${pJson.length}', style: const TextStyle(fontSize: 25)),
              Text(  pJson[round]['pregunta'], textAlign: TextAlign.center, style: const TextStyle(  fontSize: 25, fontWeight: FontWeight.w500),),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 0);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple,  minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][0]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 1);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][1]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 2);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][2]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    checkAnswer(round, 3);
                  },
                  style: ElevatedButton.styleFrom( backgroundColor: Colors.deepPurple, minimumSize: const Size(200, 60),
                  ),
                  child: Text( pJson[round]['respuestas'][3]['respuesta'], style: const TextStyle(fontSize: 20),
                  ),
                ),
              ]
            )
          ),
        ),
      ),
    );
  }
}
