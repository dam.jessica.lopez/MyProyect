import 'package:flutter/material.dart';
import '../../components_views/bar_menu.dart';
import '../../components_views/level_button.dart';

import 'Level1/phrase1.dart';
import 'Level2/phrase2.dart';
import 'Level3/phrase3.dart';


class NivelesFrasesView extends StatefulWidget{
  const NivelesFrasesView({super.key});

  @override
  State<StatefulWidget> createState() {
    return _NivelesFrasesView();
  }
}


class _NivelesFrasesView extends State<NivelesFrasesView>{
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text('FRASE',textAlign: TextAlign.center,style: TextStyle(fontSize: 30)),
        backgroundColor: Colors.purple,
        elevation: 30,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const BarMenu(),
              ),
            );
          },
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.info),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Información'),
                    content: const Text('Este juego esta basado en definiciones.\n\nConsta de una pantalla donde se muestran varias definiciones con varios botones con las respuestas.\n\nSolamente se podrá marcar una opción por definición. \nTras marcar las 6 respuestas que se creen correctas se mostrará una puntuación.'),
                    actions: [
                      TextButton(
                        child: const  Text('Cerrar'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
      ),
      backgroundColor: Colors.lightBlue,
      body:SafeArea(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
              height: 60,
            ),

            LevelButton(text: "NIVEL 1", customColor: Colors.purple.shade300, onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>  const Frase1(),
                ),
              );
            }),

            LevelButton(text: "NIVEL 2", customColor: Colors.purple, onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>  const Frase2(),
                ),
              );
            }),

            LevelButton(text: "NIVEL 3", customColor: Colors.purple.shade800, onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>   const Frase3(),
                ),
              );
            }),

          ],
        ),
      ),
    );
  }
}