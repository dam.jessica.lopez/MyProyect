import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/Piano/view.dart';

import 'design.dart';

void main() {
  runApp(Piano());
}

class Piano extends StatelessWidget {
  const Piano({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.brown,
            title: Center(child: Text("Piano")),
          ),
          body: Stack(children: [
            Container(

              height: double.infinity,
              child: ExpandedTile(),
            ),
            Desing()
          ],)
      ),
    );
  }
}