import 'package:flutter/material.dart';
import 'package:my_proyect/views/games_views/Simon/Levels/Level2/Simon2.dart';
import 'package:my_proyect/views/games_views/Simon/Levels/Level3/Simon3.dart';
import '../../../components_views/bar_menu.dart';
import '../../../components_views/level_button.dart';
import '../../../style_views/hex_color.dart';
import 'Level1/Simon.dart';


class NivelesSimonView extends StatefulWidget{
  const NivelesSimonView({super.key});

  @override
  State<StatefulWidget> createState() {
    return _NivelesSimonViewView();
  }
}


class _NivelesSimonViewView extends State<NivelesSimonView>{
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('SIMON',textAlign: TextAlign.center,style: TextStyle(fontSize: 30)),
          backgroundColor: Colors.purple,
          elevation: 30,
          leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const BarMenu(),
                  ),
                );
              },
          ),
          actions: [
          IconButton(
            icon: const Icon(Icons.info),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Información'),
                    content: const Text('Este juego esta basado en ejercitar la memoria.\n\nConsta de una pantalla donde se muestran 4 botones de diferentes colores".\n\nPara avanzar a la siguiente secuencia deberá repetir la que nos proporciona el juego con exactitud.\nTras fallar alguna de las secuencias se mostrará una puntuación y se terminará la partida.'),
                    actions: [
                      TextButton(
                        child: const  Text('Cerrar'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
        ),
        backgroundColor: Colors.lightBlue,
        body: SafeArea(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const SizedBox(
                height: 60,
              ),
    
              LevelButton(text: "NIVEL 1", customColor: Colors.purple.shade300, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>  SimonSays(),
                  ),
                );
              }),
    
              LevelButton(text: "NIVEL 2", customColor: Colors.purple, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>  SimonSays2(),
                  ),
                );
              }),
    
              LevelButton(text: "NIVEL 3", customColor: Colors.purple.shade800, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>  SimonSays3(),
                  ),
                );
              }),
    
    
    
    
    
              /*Container(
                margin: EdgeInsets.all(70),
                child:
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.purpleAccent, // background
                    onPrimary: Colors.black,
                    shadowColor: Colors.black,//
                    elevation: 7.0, // foreground
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                                builder: (context) => new SimonSays(),
                              ),
                    );
    
                  },
                  child: Text('NIVEL 1',style: TextStyle(fontSize: 60)),
                ),
              ),
    
              Container(
                margin: EdgeInsets.all(70),
                child:
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.purpleAccent, // background
                    onPrimary: Colors.black,
                    shadowColor: Colors.black,//
                    elevation: 7.0,// foreground
                  ),
                  onPressed: () { },
                  child: Text('NIVEL 2',style: TextStyle(fontSize: 60)),
                ),
              ),
              Container(
                margin: EdgeInsets.all(70),
                child:
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.purpleAccent, // background
                    onPrimary: Colors.black,
                    shadowColor: Colors.black,//
                    elevation: 7.0,// foreground
                  ),
                  onPressed: () { },
                  child: Text('NIVEL 3',style: TextStyle(fontSize: 60)),
                ),
              ),
    */
    
            ],
          ),
        ),
    
      ),
    );
  }
}