import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/views/components_views/bar_menu.dart';
import 'package:my_proyect/views/games_views/Simon/Levels/levels_simon_view.dart';

import '../../../../../controller/singleton/DataHolder.dart';
import '../../../../../model/PUGame.dart';
import '../../../../../model/Partida.dart';
import '../../../../../model/PerfilUser.dart';

class SimonSays extends StatefulWidget {
  const SimonSays({super.key});

  @override
  _SimonSaysState createState() => _SimonSaysState();
}

class _SimonSaysState extends State<SimonSays> {
  final List<int> _gameSequence = [];
  int _userStep = 0;

  int _countdown = 3;
  Timer? _countdownTimer;

  final level = 1;
  final String key = "SIMON";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;

  @override
  void initState() {
    super.initState();
    _startCountdown();
    getKeyGame();
    
  }
  void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      //print('Clave:' + key + ' ' + pUG.uid);
      
    } else {
      //print('No existe la clave');
    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = _gameSequence.length * 100;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }

  void _startCountdown() {
    const oneSec =  Duration(seconds: 1);
    _countdownTimer = Timer.periodic(
      oneSec,
      (Timer timer) {
        setState(() {
          if (_countdown < 1) {
            timer.cancel();
            _addNextStepToSequence();
          } else {
            _countdown = _countdown - 1;
          }
        });
      },
    );
  }

  @override
  void dispose() {
    _countdownTimer?.cancel();
    super.dispose();
  }

  void _addNextStepToSequence() {
    Random random =  Random();
    int nextStep = random.nextInt(4);
    setState(() {
      _gameSequence.add(nextStep);
    });
    _showGameSequence();
  }

  void _showGameSequence() {
  int stepCount = 0;
  int delay = 700;
  for (int step in _gameSequence) {
    Future.delayed(Duration(milliseconds: delay), () {
      setState(() {
        _highlightButton(step);
      });
      Future.delayed(const Duration(milliseconds: 400), () {
        setState(() {
          _resetButtonColors();
        });
      });
    });
    delay += 900;
    stepCount++;
    if (stepCount == _gameSequence.length) {
      Future.delayed(Duration(milliseconds: delay), () {
        setState(() {
          _resetButtonColors();
        });
      });
    }
  }
}


  void _highlightButton(int buttonIndex) {
    switch (buttonIndex) {
      case 0:
        setState(() {
          _button1Color = Colors.blue;
        });
        break;
      case 1:
        setState(() {
          _button2Color = Colors.green;
        });
        break;
      case 2:
        setState(() {
          _button3Color = Colors.yellow;
        });
        break;
      case 3:
        setState(() {
          _button4Color = Colors.red;
        });
        break;
    }
  }

  void _resetButtonColors() {
    setState(() {
      _button1Color = Colors.blue[100];
      _button2Color = Colors.green[100];
      _button3Color = Colors.yellow[100];
      _button4Color = Colors.red[100];
    });
  }

  void _checkUserInput(int selectedStep) {
    if (_gameSequence[_userStep] == selectedStep) {
      setState(() {
        _userStep++;
      });
      if (_userStep == _gameSequence.length) {
        _userStep = 0;
        Future.delayed(const Duration(milliseconds: 500), () {
          _addNextStepToSequence();
        });
      }
    } else {
      _showGameOverDialog();
    }
  }

  void _showGameOverDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("¡FELICIDADES!", textAlign: TextAlign.center, style: TextStyle(fontSize: 27),),
            content: Text("Puntiación: ${_gameSequence.length * 100}. ¿Quieres volver a los niveles?",
                textAlign: TextAlign.center, style: TextStyle(fontSize: 23)),
            actions: <Widget>[
              TextButton(
                child: const Text("SI", style: TextStyle(fontSize: 25)),
                onPressed: () {
                  saveMatch();
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const NivelesSimonView(),
                    ),
                  );
                },
              ),
              TextButton(
                child: const Text("NO", style: TextStyle(fontSize: 25)),
                onPressed: () {
                  saveMatch();
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const BarMenu(),
                    ),
                  );
                },
              )
            ],
          );
        });
  }

  Color? _button1Color = Colors.blue[100];
  Color? _button2Color = Colors.green[100];
  Color? _button3Color = Colors.yellow[100];
  Color? _button4Color = Colors.red[100];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('OBSERVA',textAlign: TextAlign.center, style: TextStyle(fontSize: 30)),
        backgroundColor: Colors.purple,
        elevation: 10,
      ),
      body: Center(
        child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('REPITE LA SECUENCIA',textAlign: TextAlign.center,style: TextStyle(fontSize: size.width * 0.08, fontWeight: FontWeight.bold)),
          const SizedBox(height: 30),
          _countdown > 0
                ? Text(
                    'Comenzando en $_countdown',
                    style: TextStyle(fontSize: size.width * 0.08),
                  )
                : Text(
                    '¡A Jugar!',
                    style: TextStyle(fontSize: size.width * 0.08),
                  ),
                  const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  _checkUserInput(0);
                },
                child: Container(
                  width: size.width * 0.3,
                  height: size.height * 0.2,
                  decoration: BoxDecoration(
                    color: _button1Color,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              const SizedBox(width: 20.0),
              GestureDetector(
                onTap: () {
                  _checkUserInput(1);
                },
                child: Container(
                  width: size.width * 0.3,
                  height: size.height * 0.2,
                  decoration: BoxDecoration(
                    color: _button2Color,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  _checkUserInput(2);
                },
                child: Container(
                  width: size.width * 0.3,
                  height: size.height * 0.2,
                  decoration: BoxDecoration(
                    color: _button3Color,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              const SizedBox(width: 20.0),
              GestureDetector(
                onTap: () {
                  _checkUserInput(3);
                },
                child: Container(
                  width: size.width * 0.3,
                  height: size.height * 0.2,
                  decoration: BoxDecoration(
                    color: _button4Color,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      ),
    );
  }
}

