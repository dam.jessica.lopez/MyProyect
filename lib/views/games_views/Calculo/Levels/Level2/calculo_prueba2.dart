import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import '../../../../../controller/admin_games.dart';
import '../../../../../controller/singleton/DataHolder.dart';
import '../../../../../model/PUGame.dart';
import '../../../../../model/Partida.dart';
import '../../../../../model/PerfilUser.dart';
import '../../../../components_views/bar_menu.dart';
import '../levels_calculo_view.dart';

class CalculoPrueba2 extends StatefulWidget {
  const CalculoPrueba2({Key? key}) : super(key: key);

  @override
  State<CalculoPrueba2> createState() => _CalculoPrueba2State();
}

class _CalculoPrueba2State extends State<CalculoPrueba2> {
  late int num1;
  late int num2;
  late String operator;
  late int answer;
  int pTotal = 0;
  int thisRound = 1;
  int pRound = 0;

  final level = 2;
  final String key = "CALCULA";

  Partida p = Partida();
  PUGame pUG = PUGame();
  PerfilUser perfilUser = DataHolder().perfilUser;
  

  @override
  void initState() {
    super.initState();
    generateOperation();
    getKeyGame(); 
  }

  void getKeyGame() {
    if (DataHolder().perfilUser.games.containsKey(key)) {
      pUG = DataHolder().perfilUser.games[key]!;
      print('Clave:$key ${pUG.uid}');
      
    } else {
          print('No existe la clave');

    }
  }

  void saveMatch() {
    p.fecha = Timestamp.now();
    p.puntos = pTotal;
    p.level = level;
    pUG.addMatch(perfilUser.uid, pUG.uid, p);
  }

  void generateOperation() {
    var random = Random();
    num1 = random.nextInt(150) + 1;
    num2 = random.nextInt(150) + 1;
    int operatorIdx = random.nextInt(2);
    if (operatorIdx == 0) {
      operator = '+';
      answer = num1 + num2;
    } else {
      operator = '-';
      answer = num1 - num2;
    }
  }

  void checkAnswer(int valor) {
    if (valor == answer) {
      pRound += 20;
      pTotal += 20;
    }
    if (thisRound < 20) {
      setState(() {
        generateOperation();
        thisRound++;
      });
    } else {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('¡HAS TERMINADO!', textAlign: TextAlign.center, style: TextStyle(fontSize: 27)),
            content: Text(
                'Puntuación total: $pTotal,\n ¿Quiéres volver a los niveles?', textAlign: TextAlign.center, style: TextStyle(fontSize: 23)),
            actions: [
              TextButton(
                onPressed: () {
                  saveMatch();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => NivelesCalculoView(),
                    ),
                  );
                },
                child: const Text('SI', style: TextStyle(fontSize: 25)),
              ),
              TextButton(
                onPressed: () { 
                  saveMatch();
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const BarMenu(),
                  ));
                },
                child: const Text('NO', style: TextStyle(fontSize: 25)),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('¡A CALCULAR!',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30)),
        backgroundColor: Colors.deepPurple,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>  NivelesCalculoView(),
              ),
            );
          },
        ),
      ),
      backgroundColor: Colors.deepPurple[100],
      body: SafeArea(
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.5,
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Ronda: $thisRound / 20',
                    style: TextStyle(fontSize: size.width * 0.07)),
                const SizedBox(height: 20),
                Text('¿Cuánto es $num1 $operator $num2?',
                    style: TextStyle(fontSize: size.width * 0.08)),
                const SizedBox(height: 20),
                ElevatedButton(
                    onPressed: () => checkAnswer(num1 + num2),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.deepPurple,
                      // Ajusta el padding interno del botón
                      minimumSize: const Size(
                          200, 60), // Define el tamaño mínimo del botón
                    ),
                    child: Text('${num1 + num2}',
                        style: TextStyle(fontSize: size.width * 0.09))),
                ElevatedButton(
                    onPressed: () => checkAnswer(num1 - num2),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.deepPurple,
                     // Ajusta el padding interno del botón
                      minimumSize: const Size(
                          200, 60), // Define el tamaño mínimo del botón
                    ),
                    child: Text('${num1 - num2}',
                        style: TextStyle(fontSize: size.width * 0.09))),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
