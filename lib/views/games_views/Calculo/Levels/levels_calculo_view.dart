
import 'package:flutter/material.dart';
import 'package:my_proyect/views/components_views/level_button.dart';
import 'package:my_proyect/views/games_views/Calculo/Levels/Level1/calculo_prueba.dart';
import '../../../components_views/bar_menu.dart';
import 'Level2/calculo_prueba2.dart';
import 'Level3/calculo_prueba3.dart';

class NivelesCalculoView extends StatefulWidget {

  String uid; // Recibe el uid del usuario
  NivelesCalculoView({super.key, this.uid= ''});

  @override
  State<StatefulWidget> createState() {
    return _NivelesCalculoView();
  }
}

class _NivelesCalculoView extends State<NivelesCalculoView> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('CÁLCULO',
              textAlign: TextAlign.center, style: TextStyle(fontSize: 30)),
          backgroundColor: Colors.purple,
          elevation: 30,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const BarMenu(),
              ));
            },
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.info),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Información'),
                      content: const Text('Este juego esta basado en operaciones sencillas.\n\nConsta de una pantalla donde se muestra una operación que se deberá resolver y dos botones con las respuestas.\n\nPara avanzar a la siguiente operacion deberá presionar uno de los dos botones. \nTras acabar las 20 rondas se mostrará una puntuación.'),
                      actions: [
                        TextButton(
                          child: const  Text('Cerrar'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          ],
        ),
        backgroundColor: Colors.lightBlue,
        body: SafeArea(
          child: Column(
            children: [
              const SizedBox(
                height: 60,
              ),
              LevelButton(text: "NIVEL 1", customColor: Colors.purple.shade300, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CalculoPrueba(),
                  ),
                );
              }),
              
              LevelButton(text: "NIVEL 2", customColor: Colors.purple, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CalculoPrueba2(),
                  ),
                );
              }),
    
              LevelButton(text: "NIVEL 3", customColor: Colors.purple.shade800, onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CalculoPrueba3(),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
