import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import 'package:my_proyect/views/profesional_profile_views/add_user.dart';
import 'package:my_proyect/views/profesional_profile_views/user_detail.dart';
import '../../model/PerfilUser.dart';
import '../components_views/list_tile_user.dart';

class ListUser extends StatefulWidget {
  const ListUser({super.key});

  @override
  State<ListUser> createState() => _ListUserState();
}

class _ListUserState extends State<ListUser> {
late List<PerfilUser> docsProfile;

  //ordenar por fecha de creacion
  final Stream<QuerySnapshot> _listUser = DataHolder()
      .db
      .collection('usuarios')
      .where('isActive', isEqualTo: true)
      .orderBy('createdDate', descending: true)
      .snapshots();

      

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('PACIENTES',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: screenSize.width * 0.07)),
        backgroundColor: Colors.purple.shade700,
        elevation: 10,
        actions: [
          IconButton(
            onPressed: () {
              showSearch(
                context: context, 
                delegate: CustomSearchDelegate(searchDocs: docsProfile));
            },
            icon: const Icon(Icons.search),
          ),
        ],
      ),
      backgroundColor: Colors.deepPurple.shade100,
      body: StreamBuilder<QuerySnapshot>(
          stream: _listUser,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return const Center(child: Text('Error al cargar los datos', 
              textAlign: TextAlign.center, 
              style: TextStyle(fontSize: 30, color: Colors.blue)));
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  child: CircularProgressIndicator(
                    color: Colors.purple,
                  ),
                ),
              );
            }
            
            docsProfile = snapshot.data!.docs
                .map((doc) => PerfilUser.fromFirestore(
                      doc as DocumentSnapshot<
                          Map<String, dynamic>>, // Realizar el casting
                      null, // Pasar el parámetro options si es necesario
                    ))
                .toList();

            return ListView.builder(
              itemCount: docsProfile.length,
              itemBuilder: (BuildContext context, int index) {
                final user = docsProfile[index];
                return ListTileUser(
                  icon: Icons.person_pin,
                  onLongPress: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('¿Dar de baja?', textAlign: TextAlign.center, style: TextStyle(fontSize: 20)),
                          content: const Text(
                              '¿Estás seguro de que quieres dar de baja este paciente?',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 18)),
                          actions: [
                            TextButton(
                              onPressed: () {
                                DataHolder().db
                                    .collection('usuarios')
                                    .doc(user.uid)
                                    .update({'isActive': false});
                                Navigator.pop(context);
                              },
                              child: const Text('SI'),
                            ),
                            TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text('NO'),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => UserDetail(
                          doc: user,
                        ),
                      ),
                    );
                  },
                  text: user.name == ""
                      ? '${user.phone!.substring(3, 6)} ${user.phone!.substring(6, 8)} ${user.phone!.substring(8, 10)} ${user.phone!.substring(10)}'
                      : user.name!,
                  trailing: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AddUser(
                          doc: user,
                        ),
                      ),
                    );
                  },
                );
              },
            );
          }),
    );
  }
}

class CustomSearchDelegate  extends SearchDelegate{
  

  final List<PerfilUser> searchDocs;

  CustomSearchDelegate({required this.searchDocs});

  @override
  List<Widget>? buildActions(BuildContext context) {
   return [
     IconButton(
       onPressed: () {
         query = '';
       }, 
       icon: const Icon(Icons.clear)
     )
   ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(context, null);
      }, 
      icon: const Icon(Icons.arrow_back)
    );
  }

 @override
Widget buildResults(BuildContext context) {
  List<PerfilUser> filteredList = searchDocs
      .where((element) => element.name!.toLowerCase().contains(query.toLowerCase()) || element.phone!.contains(query))
      .toList();

  return ListView.builder(
    itemCount: filteredList.length,
    itemBuilder: (BuildContext context, int index) {
      final user = filteredList[index];
      return ListTile(
        title: Text(user.name!),
        subtitle: Text(user.phone!),
        onTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => UserDetail(
                doc: user,
              ),
            ),
          );
        },
      );
    },
  );
}

@override
Widget buildSuggestions(BuildContext context) {
  List<PerfilUser> filteredList = searchDocs
      .where((element) => element.name!.toLowerCase().contains(query.toLowerCase()) || element.phone!.contains(query))
      .toList();

  return ListView.builder(
    itemCount: filteredList.length,
    itemBuilder: (BuildContext context, int index) {
      final user = filteredList[index];
      return ListTile(
        title: Text(user.name!),
        subtitle: Text(user.phone!),
        onTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => UserDetail(
                doc: user,
              ),
            ),
          );
        },
      );
    },
  );
}

}
