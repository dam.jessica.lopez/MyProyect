import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_proyect/model/PerfilUser.dart';
import 'package:my_proyect/model/PUGame.dart';
import 'package:my_proyect/model/Partida.dart';
import 'package:collection/collection.dart';
import 'package:supercharged/supercharged.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class UserDetail extends StatefulWidget {
  const UserDetail({Key? key, this.doc}) : super(key: key);

  final PerfilUser? doc;

  @override
  State<UserDetail> createState() => _UserDetailState();
}

class _UserDetailState extends State<UserDetail> {
  late Future<Map<String, Map<DateTime, int>>> statisticsFuture;
  Map<String, Map<DateTime, int>> puntuacionesAcumuladas = {};

  @override
  void initState() {
    super.initState();
    statisticsFuture = getStatistics();
  }

  Future<Map<String, Map<DateTime, int>>> getStatistics() async {
    await widget.doc!.getGamesFromFB();

    Map<String, Map<DateTime, List<int>>> partidasAgrupadas = {};

    if (widget.doc!.games != null) {
      for (final game in widget.doc!.games.values) {
        await game.getPartidasWeekFromFB(widget.doc!.uid);
        // Agrupar las partidas por juego y por día
        for (final partida in game.partidas) {
          final juego = game.name ?? '';
          final date = partida.fecha!.toDate();
          final day = DateTime(date.year, date.month, date.day);

          if (!partidasAgrupadas.containsKey(juego)) {
            partidasAgrupadas[juego] = {};
          }

          if (!partidasAgrupadas[juego]!.containsKey(day)) {
            partidasAgrupadas[juego]![day] = [];
          }

          partidasAgrupadas[juego]![day]!.add(partida.puntos ?? 0);
        }
      }
    }

    // Calcular la puntuación acumulada por día

    partidasAgrupadas.forEach((juego, partidasPorDia) {
      puntuacionesAcumuladas[juego] = {};
      partidasPorDia.forEach((day, puntuaciones) {
        int puntuacionTotal =
            puntuaciones.fold(0, (sum, puntos) => sum + puntos);
        puntuacionesAcumuladas[juego]![day] = puntuacionTotal;
      });
    });

    return puntuacionesAcumuladas;
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.doc!.name != "" ? widget.doc!.name! : widget.doc!.phone ?? '',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: screenSize.width * 0.07),
        ),
        backgroundColor: Colors.purple.shade700,
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blue.shade200,
              Colors.blue.shade100,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: FutureBuilder<Map<String, Map<DateTime, int>>>(
          future: statisticsFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text('Error: ${snapshot.error}'),
              );
            } else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
              Map<String, Map<DateTime, int>> partidasAgrupadas =
                  snapshot.data!;
              return ListView.builder(
                itemCount: partidasAgrupadas.length,
                itemBuilder: (context, index) {
                  final juego = partidasAgrupadas.keys.toList()[index];
                  final partidasPorDia = partidasAgrupadas[juego]!;
                  return Card(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            juego,
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                            ),
                          ),
                          const SizedBox(height: 8),
                          ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: partidasPorDia.length,
                            itemBuilder: (context, index) {
                              final day = partidasPorDia.keys.toList()[index];
                              final puntuacionTotal = partidasPorDia[day]!;
                              return ListTile(
                                title: Text(
                                    'Día ${DateFormat('dd/MM/yyyy').format(day)}: $puntuacionTotal',
                                    style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    )),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            } else {
              return const Center(
                child: Text(
                  'No hay datos disponibles',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              );
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.large(
        onPressed: () {
          // Navegar a otra vista al pulsar el botón
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AnotherView(
                data: puntuacionesAcumuladas,
              ),
            ),
          );
        },
        backgroundColor: Colors.purple.shade700,
        child: const Icon(Icons.bar_chart, size: 50),
      ),
    );
  }
}


class AnotherView extends StatefulWidget {
  final Map<String, Map<DateTime, int>>? data;

  AnotherView({Key? key, this.data}) : super(key: key);

  @override
  State<AnotherView> createState() => _AnotherViewState();
}

class _AnotherViewState extends State<AnotherView> {
  List<ChartData> chartData = [];

  @override
  void initState() {
    super.initState();
    getListData();
  }

  void getListData() {
    widget.data!.forEach((juego, partidasPorDia) {
      partidasPorDia.forEach((day, puntuacion) {
        chartData.add(ChartData(juego, day, puntuacion));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Gráfico de barras'),
        backgroundColor: Colors.purple.shade700,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: SfCartesianChart(
              enableSideBySideSeriesPlacement: true,
              primaryXAxis: DateTimeAxis(
                dateFormat: DateFormat('dd/MM'),
                edgeLabelPlacement: EdgeLabelPlacement.shift,
                intervalType: DateTimeIntervalType.days,
                interval: 1,
              ),
              title: ChartTitle(
                text: 'Estadísticas de juegos',
                textStyle: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              legend: Legend(isVisible: true),
              tooltipBehavior: TooltipBehavior(enable: true),
              series: widget.data!.keys.map((juego) {
                final juegoData = chartData.where((data) => data.juego == juego).toList();
                return ColumnSeries<ChartData, DateTime>(
                  dataSource: juegoData,
                  xValueMapper: (ChartData data, _) => data.day,
                  yValueMapper: (ChartData data, _) => data.puntuacion,
                  sortingOrder: SortingOrder.descending,
                  sortFieldValueMapper: (ChartData data, _) => data.puntuacion,
                  name: juego,
                  dataLabelSettings: const DataLabelSettings(
                    isVisible: true,
                    labelAlignment: ChartDataLabelAlignment.top,
                    textStyle: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  width: 0.9,
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}

class ChartData {
  ChartData(this.juego, this.day, this.puntuacion);

  final String juego;
  final DateTime day;
  final int puntuacion;
}

