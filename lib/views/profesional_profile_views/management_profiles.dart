import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import 'package:my_proyect/model/PerfilPro.dart';
import 'package:my_proyect/model/PerfilUser.dart';
import 'package:my_proyect/views/profesional_profile_views/profesional_profile.dart';

import '../../controller/singleton/DataHolder.dart';
import '../components_views/list_tile_user.dart';
import '../components_views/snack.dart';

class ManagementProfiles extends StatefulWidget {
  const ManagementProfiles({super.key});

  @override
  State<ManagementProfiles> createState() => _ManagementProfilesState();
}

class _ManagementProfilesState extends State<ManagementProfiles>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController tabController = TabController(length: 2, vsync: this);

    final Stream<QuerySnapshot> _listUser = DataHolder()
        .db
        .collection('usuarios')
        .where('isActive', isEqualTo: true)
        .orderBy('createdDate', descending: true)
        .snapshots();

    final Stream<QuerySnapshot> _listProfesional =
        DataHolder().db.collection('profesionales').snapshots();

    @override
    void initState() {
      super.initState();
    }

    @override
    void dispose() {
      tabController.dispose();
      super.dispose();
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("GESTIÓN DE PERFILES"),
        backgroundColor: Colors.purple.shade800,
        elevation: 10,
        leading: IconButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const ProfesionalProfile(),
              ),
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
        bottom: TabBar(
          controller: tabController,
          tabs: const [
            Tab(text: 'PROFESIONALES'),
            Tab(text: 'USUARIOS'),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          // View for Professionals
          StreamBuilder<QuerySnapshot>(
              stream: _listProfesional,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return const Center(
                      child: Text('Error al cargar los datos',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 30, color: Colors.blue)));
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      child: CircularProgressIndicator(
                        color: Colors.purple,
                      ),
                    ),
                  );
                }

                List<PerfilPro> docsProfile = snapshot.data!.docs
                    .map((doc) => PerfilPro.fromFirestore(
                          doc as DocumentSnapshot<
                              Map<String, dynamic>>, // Realizar el casting
                          null, // Pasar el parámetro options si es necesario
                        ))
                    .toList();

                return ListView.builder(
                  itemCount: docsProfile.length,
                  itemBuilder: (BuildContext context, int index) {
                    final prof = docsProfile[index];
                    return GestureDetector(
                      onLongPress: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('¿Eliminar profesional?',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 20)),
                              content: const Text(
                                  '¿Estás seguro de que quieres eliminar este profesional?',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18)),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    if (prof.uid ==
                                        DataHolder().auth.currentUser!.uid) {
                                      showSnackBar(context,
                                          'No puedes eliminar tu propio perfil');
                                      Navigator.pop(context);
                                    } else if (prof.isAdmin!) {
                                      showSnackBar(
                                        context,
                                        'No puedes eliminar un administrador',
                                      );
                                      Navigator.pop(context);
                                    } else {
                                      FBAdmin()
                                          .deleteProfesional(prof.uid, context)
                                          .then((value) => {
                                                showSnackBar(context,
                                                    "Perfil eliminado correctamente"),
                                                Navigator.pop(context)
                                              })
                                          .catchError((error) => {
                                                showSnackBar(context,
                                                    "Error al eliminar el perfil del profesional")
                                              });
                                    }
                                  },
                                  child: const Text('SI'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text('NO'),
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: ExpansionTile(
                        title: Text(
                          prof.name == ""
                              ? 'Profesional'
                              : '${prof.name!} ${prof.surname!}',
                          style: const TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                         children: [
                          Column(
                            children: [
                              Text(
                                  'Profesión: ${prof.profession!}',
                                  style: const TextStyle(fontSize: 16)),
                              const SizedBox(height: 10),
                              Text('Administrador: ${prof.isAdmin! ? 'Sí' : 'No'}',
                                  style: const TextStyle(fontSize: 16)),
                              const SizedBox(height: 10),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                );
              }),

          // View for Users
          StreamBuilder<QuerySnapshot>(
              stream: _listUser,
              builder:
                  (BuildContext ctx, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return const Center(
                      child: Text('Error al cargar los datos',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 30, color: Colors.blue)));
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      child: CircularProgressIndicator(
                        color: Colors.purple,
                      ),
                    ),
                  );
                }

                List<PerfilUser> docsProfile = snapshot.data!.docs
                    .map((doc) => PerfilUser.fromFirestore(
                          doc as DocumentSnapshot<
                              Map<String, dynamic>>, // Realizar el casting
                          null, // Pasar el parámetro options si es necesario
                        ))
                    .toList();

                return ListView.builder(
                  itemCount: docsProfile.length,
                  itemBuilder: (BuildContext ctx, int index) {
                    final user = docsProfile[index];
                    return GestureDetector(
                      onLongPress: () {
                        showDialog(
                          context: ctx,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('¿Eliminar paciente?',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 20)),
                              content: const Text(
                                  '¿Estás seguro de que quieres eliminar este paciente?',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18)),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    FBAdmin()
                                        .deleteUserAndSubcollections(user.uid)
                                        .then((value) => {
                                              showSnackBar(ctx,
                                                  "Perfil eliminado correctamente"),
                                              Navigator.pop(ctx)
                                            })
                                        .catchError((error) => {
                                              showSnackBar(ctx,
                                                  "Error al eliminar el perfil del paciente")
                                            });
                                  },
                                  child: const Text('SI'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(ctx);
                                  },
                                  child: const Text('NO'),
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: ExpansionTile(
                        title: Text(
                          user.name == ""
                              ? '${user.phone!.substring(3, 6)} ${user.phone!.substring(6, 8)} ${user.phone!.substring(8, 10)} ${user.phone!.substring(10)}'
                              : user.name!,
                          style: const TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        children: [
                          Column(
                            children: [
                              Text(
                                  'Alta: ${user.createdDate!.toDate().toString().substring(0, 16)}',
                                  style: const TextStyle(fontSize: 16)),
                              const SizedBox(height: 10),
                              Text('Teléfono: ${user.phone!}',
                                  style: const TextStyle(fontSize: 16)),
                              const SizedBox(height: 10),
                              Text('Edad: ${calculateAge(user.age!)} años',
                                  style: const TextStyle(fontSize: 16)),
                              const SizedBox(height: 10),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                );
              }),
        ],
      ),
    );
  }

  String calculateAge(Timestamp timestamp) {
    DateTime birthday = timestamp.toDate();
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthday.year;
    int month1 = currentDate.month;
    int month2 = birthday.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthday.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }
}
