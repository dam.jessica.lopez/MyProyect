import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_proyect/model/PerfilUser.dart';
import 'package:my_proyect/views/components_views/custom_button.dart';
import '../../controller/FbAdmin.dart';
import '../components_views/input_text.dart';

class AddUser extends StatefulWidget {
  const AddUser({super.key, this.doc});

  final PerfilUser? doc;

  @override
  State<AddUser> createState() => _AddUserState();
}

final List<String> options = ['Hombre', 'Mujer'];

class _AddUserState extends State<AddUser> {
  var _currentSelectedDate;

  final _formKey = GlobalKey<FormState>();

  String currentOption = options[0];

  TextEditingController nombreController = TextEditingController();
  TextEditingController nacimientoController = TextEditingController();
  TextEditingController apellidoController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _currentSelectedDate = DateTime.now();
    nacimientoController.text =
        _currentSelectedDate.toString().substring(0, 10);
  }

  void callDatePicker() async {
    var selectedDate = await getDate();
    setState(() {
      _currentSelectedDate = selectedDate;
      nacimientoController.text =
          _currentSelectedDate.toString().substring(0, 10);
      print(_currentSelectedDate);
    });
  }

  Future<DateTime?> getDate() {
    return showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime(2100),
        builder: (BuildContext context, Widget? child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: const ColorScheme.light(
                primary: Colors.purple,
                onPrimary: Colors.white,
                surface: Colors.purple,
                onSurface: Colors.black,
              ),
              dialogBackgroundColor: Colors.white,
            ),
            child: child!,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('EDITAR PERFIL',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: screenSize.width * 0.07)),
        backgroundColor: Colors.purple.shade700,
        elevation: 10,
      ),
      backgroundColor: Colors.deepPurple.shade100,
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          padding: MediaQuery.of(context).size.height > 800
              ? const EdgeInsets.symmetric(vertical: 50, horizontal: 0)
              : const EdgeInsets.symmetric(vertical: 30, horizontal: 0),
          child: Center(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
              ),
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.8,
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InputText(
                      controller: nombreController,
                      titulo: "NOMBRE",
                      icono: const Icon(Icons.person, color: Colors.blue, size: 30),
                    ),
                    InputText(
                      controller: apellidoController,
                      titulo: "APELLIDOS",
                      icono: const Icon(Icons.email, color: Colors.blue, size: 30),
                    ),
                    InputText(
                      controller: nacimientoController,
                      titulo: "NACIMIENTO",
                      icono: const Icon(Icons.cake, color: Colors.blue, size: 30),
                      iconButton: IconButton(
                        icon: const Icon(Icons.calendar_today,
                            color: Colors.purple),
                        onPressed: () {
                          callDatePicker();
                          nacimientoController.text =
                              _currentSelectedDate.toString();
                        },
                      ),
                    ),
                    Row(children: [
                      Expanded(
                        child: RadioListTile(
                          contentPadding: const EdgeInsets.all(0),
                          activeColor: Colors.purple,
                          title: Text(options[0], style: TextStyle(fontSize: screenSize.width * 0.06),),
                          groupValue: currentOption,
                          value: options[0],
                          onChanged: (String? value) {
                            setState(() {
                              debugPrint("------->value: $value");
                              currentOption = value!;
                            });
                          },
                        ),
                      ),
                      Expanded(
                        child: RadioListTile(
                          contentPadding: const EdgeInsets.all(0),
                          activeColor: Colors.purple,
                          title: Text(options[1], style: TextStyle(fontSize: screenSize.width * 0.06)),
                          groupValue: currentOption,
                          value: options[1],
                          onChanged: (String? value) {
                            setState(() {
                              debugPrint("------->value: $value");
                              currentOption = value!;
                            });
                          },
                        ),
                      ),
                    ]),
                    CustomButton(
                      text: 'ACEPTAR',
                      color: Colors.purple.shade700,
                      action: () {
                        if (_formKey.currentState!.validate()) {
                          FBAdmin().updateProfileUser(
                              widget.doc?.uid,
                              nombreController.text,
                              apellidoController.text,
                              currentOption.toString(),
                              Timestamp.fromDate(_currentSelectedDate),
                              context);

                              
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
