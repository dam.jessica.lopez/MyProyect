import 'package:flutter/material.dart';
import 'package:my_proyect/controller/FbAdmin.dart';
import 'package:my_proyect/controller/singleton/DataHolder.dart';
import 'package:my_proyect/views/authentication/login_view.dart';
import 'package:my_proyect/views/components_views/custom_row_profile.dart';
import 'package:my_proyect/views/profesional_profile_views/management_profiles.dart';
import '../../model/PerfilPro.dart';

class ProfesionalProfile extends StatefulWidget {
  const ProfesionalProfile({super.key});

  @override
  State<ProfesionalProfile> createState() => _UsuarioViewState();
}

class _UsuarioViewState extends State<ProfesionalProfile> {
  String? imageUrl;
  PerfilPro? perfil = DataHolder().perfil;
  String? name;
  Color _color = Colors.purple.shade700;
  var size = 20;

  @override
  void initState() {
    super.initState();
    updateNameImage();
  }

  updateNameImage() {
  FBAdmin().getProfile().then((value) async => {
    setState(() {
      if (value != null) {
        perfil = value;
        name = perfil!.name;
        imageUrl = perfil!.image;
        size = perfil!.isAdmin ?? false ? 20 : 40;
      } else {
        perfil = null;
        //name = "Usuario";
        imageUrl = null;
        size = 40;
      }
    })
  });
}


  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.purple.shade800,
          automaticallyImplyLeading: false,
        ), //
        backgroundColor: Colors.deepPurple.shade100,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 7,
                        offset: Offset(0, 2),
                      ),
                    ],
                    borderRadius: const BorderRadius.vertical(
                      bottom: Radius.circular(50),
                    ),
                    gradient: LinearGradient(
                      colors: [
                        Colors.purple.shade800,
                        Colors.purple.shade700,
                        Colors.purple.shade600,
                        Colors.purple.shade500,
                        Colors.purple.shade400,
                        Colors.purple.shade300,
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  width: screenSize.width,
                  height: screenSize.height > 800 ? screenSize.height * 0.4 : screenSize.height * 0.35,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start, 
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.white24,
                        backgroundImage: imageUrl != null
                            ? NetworkImage(imageUrl!)
                            : const AssetImage('assets/images/cuenta/usuario.png')
                                as ImageProvider,
                        radius: MediaQuery.of(context).size.width * 0.2,
                      ),
                      const SizedBox(height: 10),
                      Text(
                        "$name",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: screenSize.width * 0.15,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      perfil?.isAdmin ?? false ?
                           CustomRowProfile(
                              action: () {
                                Navigator.pushNamed(context, '/man');
                              },
                              color: _color,
                              icon: Icon(Icons.person_off_rounded,
                                  color: Colors.white, size: size.toDouble()),
                              text: "GESTIÓN",
                            )
                          : const SizedBox(height: 0),
                      const SizedBox(height: 20),
                      CustomRowProfile(
                        action: () {
                          Navigator.pushNamed(context, '/onBoarding');
                        },
                        color: _color,
                        icon: Icon(Icons.settings,
                            color: Colors.white, size: size.toDouble()),
                        text: "AJUSTES",
                      ),
                      const SizedBox(height: 20),
                      perfil?.name?.toLowerCase() == "invitado" ?
                      const SizedBox(height: 0) :
                      CustomRowProfile(
                        action: () {
                          Navigator.pushNamed(context, '/listUser');
                        },
                        color: _color,
                        icon: Icon(
                          Icons.emoji_people,
                          color: Colors.white,
                          size: size.toDouble(),
                        ),
                        text: "PACIENTES",
                      ),
                      const SizedBox(height: 20),
                      CustomRowProfile(
                        action: () {
                          FBAdmin().singOut(context).then((value) => {
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const LoginView()),
                                    (route) => false)
                              });
                        },
                        color: _color,
                        icon: Icon(Icons.exit_to_app,
                            color: Colors.white, size: size.toDouble()),
                        text: "SALIR",
                      )
              
                      //ProfileMenuWidget(icon: Icons.logout,title: "Log out" ,textColor:Colors.deepOrangeAccent[100] ,endIcon: false, onPress: () {  },),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
